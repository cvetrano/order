# Orderhub

## Getting Started

To get you started, you'll need to install android studio, genymotion, and git. 

```
brew install git
```

Download and install the latest Android Studio and genymotion versions from the internet.

In addition to genymotion, you can just plug an Android tablet right into your computer and run Orderhub on that. You'll have to enable USB debugging on the tablet to debug on there though.

### Import the project into Android Studio

In Android studio, click open an existing project, and open the build.gradle folder in the orderhub-native directory. use the default gradle wrapper

### Set up genymotion

Add a virtual device that you'd like to use to test Orderhub on. Start up the vm. 
