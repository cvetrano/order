package com.grubhub.android.printing;

import android.content.res.Resources;
import android.graphics.Bitmap;

/**
 * The PrintComposer uses the builder pattern to collect print
 * commands, controls and output.
 *
 * It is a stateful composer in the sense that certain style commands
 * will be remembered and used until changed via another method call.
 *
 * This is due to the printer libraries where text commands require
 * all the attributes to be set on each text output.
 *
 */
public interface PrintComposer {

    /**
     * Add a line of text, including a line feed.
     * @param text
     * @return
     */
    PrintComposer addText(String text, PrintStyle ... style);

    /**
     * Send a print command
     * @param style
     * @return
     */
    PrintComposer addStyle(PrintStyle style);

    PrintComposer addBitmap(Bitmap bitmap);

    PrintComposer addResource(Resources resource);

}
