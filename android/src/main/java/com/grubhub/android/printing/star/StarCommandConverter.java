package com.grubhub.android.printing.star;

import com.grubhub.android.printing.PrintCommandConverter;
import com.grubhub.android.printing.PrintStyle;

import android.content.res.Resources;
import android.graphics.Bitmap;

import java.util.List;

public class StarCommandConverter implements PrintCommandConverter {
    private byte heightExpansion = '0' + 2;
    private byte widthExpansion  = '0' + 2;

    @Override
    public void convert(List<Byte> cmds, PrintStyle style) {
        byte[] cmd = null;

        switch(style) {
            case AlignLeft:
                cmd = new byte[] {0x1b, 0x1d, 0x61, 0x30};
                break;
            case AlignCenter:
                cmd = new byte[] {0x1b, 0x1d, 0x61, 0x31};
                break;
            case AlignRight:
                cmd = new byte[] {0x1b, 0x1d, 0x61, 0x32};
                break;

            case BoldOn:
                cmd = new byte[] {0x1b, 0x45};
                break;
            case BoldOff:
                cmd = new byte[] {0x1b, 0x46};
                break;

            case InvertOn:
                cmd = new byte[] {0x1b, 0x34};
                break;
            case InvertOff:
                cmd = new byte[] {0x1b, 0x35};
                break;

            case CutFull:
                cmd = new byte[] {0x1b, 0x64, 0x30};
                break;
            case CutPartial:
                cmd = new byte[] {0x1b, 0x64, 0x31};
                break;

            case LineFeed:
                cmd = new byte[] {0x0a};
                break;

            case SlashZeroOn:
                cmd = new byte[] {0x1b, 0x2f, 0x31};
                break;
            case SlashZeroOff:
                cmd = new byte[] {0x1b, 0x2f, 0x30};
                break;

            case UnderlineOn:
                cmd = new byte[] {0x1b, 0x2d, 0x31};
                break;
            case UnderlineOff:
                cmd = new byte[] {0x1b, 0x2d, 0x30};
                break;

            case WidthExtraLarge:
                widthExpansion = '0' + 5;
                break;
            case WidthLarge:
                widthExpansion = '0' + 4;
                break;
            case WidthMedium:
                widthExpansion = '0' + 3;
                break;
            case WidthSmall:
                widthExpansion = '0' + 2;
                break;
            case WidthTiny:
                widthExpansion = '0' + 1;
                break;

            case HeightExtraTall:
                heightExpansion = '0' + 5;
                break;
            case HeightTall:
                heightExpansion = '0' + 4;
                break;
            case HeightMedium:
                heightExpansion = '0' + 3;
                break;
            case HeightShort:
                heightExpansion = '0' + 2;
                break;
            case HeightTiny:
                heightExpansion = '0' + 1;
                break;
        }

        if (cmd != null) {
            for (byte b : cmd) {
                cmds.add(b);
            }
        }
    }

    @Override
    public void convert(List<Byte> cmds, String text) {
        byte[] charExpansion = new byte[] {0x1b, 0x69, heightExpansion, widthExpansion};

        for (byte b : charExpansion) {
            cmds.add(b);
        }
        for (byte b : text.getBytes()) {
            cmds.add(b);
        }
    }

    @Override
    public void convert(List<Byte> cmds, Bitmap bitmap) {
    }

    @Override
    public void convert(List<Byte> cmds, Resources res) {
    }
}
