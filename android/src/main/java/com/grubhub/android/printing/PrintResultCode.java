package com.grubhub.android.printing;

import com.starmicronics.stario.StarPrinterStatus;


public enum PrintResultCode {

    NoPrinterFound,
    EmptyPrintImage,
    PrinterOffline,
    OutOfPaperOrPaperIssues,
    CoverOpen,
    PrinterTemperature,
    UnrecoverableError,
    CutterError,
    BadPrintImage,
    Unknown;


    public static PrintResultCode getPrintResultCodeForStarPrinterStatus(StarPrinterStatus status) {

        if (status.coverOpen) {
            return CoverOpen;
        } else if (status.cutterError) {
            return CutterError;
        } else if (status.overTemp) {
            return PrinterTemperature;
        } else if (status.receiptPaperEmpty) {
            return OutOfPaperOrPaperIssues;
        } else if (status.unrecoverableError) {
            return UnrecoverableError;
        } else if (status.offline) {
            return PrinterOffline;
        } else {
            return null;
        }
    }
}
