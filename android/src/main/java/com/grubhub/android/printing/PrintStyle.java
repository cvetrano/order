package com.grubhub.android.printing;

/**
 * Print style definitions
 */
public enum PrintStyle {
    WidthExtraLarge, WidthLarge, WidthMedium, WidthSmall, WidthTiny,
    HeightExtraTall, HeightTall, HeightMedium, HeightShort, HeightTiny,
    AlignLeft, AlignCenter, AlignRight,
    BoldOn, BoldOff,
    UnderlineOn, UnderlineOff,
    SlashZeroOn, SlashZeroOff,
    InvertOn, InvertOff,
    CutFull, CutPartial,
    LineFeed
}
