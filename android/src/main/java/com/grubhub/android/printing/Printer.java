package com.grubhub.android.printing;

import com.starmicronics.stario.PortInfo;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

public class Printer implements Serializable {

    private final String ipAddress;
    private final String macAddress;
    private final String printerName;

    public Printer(String ip, String mac, String printerName) {
        this.ipAddress = ip;
        this.macAddress = mac;
        this.printerName = printerName;
    }

    public Printer(PortInfo portInfo){
        this.ipAddress = portInfo.getPortName();
        this.macAddress = portInfo.getMacAddress();
        this.printerName = portInfo.getModelName();
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getPrinterName() {
        return printerName;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Printer) {
            Printer otherPrinter = (Printer) o;
            return StringUtils.equals(ipAddress, otherPrinter.ipAddress) &&
                    StringUtils.equals(macAddress, otherPrinter.macAddress) &&
                    StringUtils.equals(printerName, otherPrinter.printerName);
        }
        
        return false;
    }
}
