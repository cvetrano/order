package com.grubhub.android.printing;

public class PrintResult {

    private boolean success;
    private PrintResultCode resultCode;
    private String printExceptionMessage;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public PrintResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(PrintResultCode resultCode) {
        this.resultCode = resultCode;
    }

    public String getPrintExceptionMessage() {
        return printExceptionMessage;
    }

    public void setPrintExceptionMessage(String printExceptionMessage) {
        this.printExceptionMessage = printExceptionMessage;
    }

}
