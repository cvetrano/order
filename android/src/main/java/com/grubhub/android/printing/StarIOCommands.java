package com.grubhub.android.printing;

public class StarIOCommands {
    public static final byte[] Init             = new byte[] {0x1b, 0x40};
    public static final byte[] OpenCashDrawer   = new byte[] {0x07};

    public static final byte[] SlashZeroOn      = new byte[] {0x1b, 0x2f, 0x31};
    public static final byte[] SlashZeroOff     = new byte[] {0x1b, 0x2f, 0x30};

    public static final byte[] UnderlineOn      = new byte[] {0x1b, 0x2d, 0x31};
    public static final byte[] UnderlineOff     = new byte[] {0x1b, 0x2d, 0x30};

    public static final byte[] UpperlineOn      = new byte[] {0x1b, 0x5f, 0x31};
    public static final byte[] UpperlineOff     = new byte[] {0x1b, 0x5f, 0x30};

    public static final byte[] EmphasizeOn      = new byte[] {0x1b, 0x45};
    public static final byte[] EmphasizeOff     = new byte[] {0x1b, 0x46};

    public static final byte[] InvertColorOn    = new byte[] {0x1b, 0x34};
    public static final byte[] InvertColorOff   = new byte[] {0x1b, 0x35};

    public static final byte[] CharExpansion    = new byte[] {0x1b, 0x69, 0x00, 0x00};

    public static final byte[] AlignLeft        = new byte[] {0x1b, 0x1d, 0x61, 0x30};
    public static final byte[] AlignCenter      = new byte[] {0x1b, 0x1d, 0x61, 0x31};
    public static final byte[] AlignRight       = new byte[] {0x1b, 0x1d, 0x61, 0x32};

    public static final byte[] CutFull          = new byte[] {0x1b, 0x64, 0x30};
    public static final byte[] CutPartial       = new byte[] {0x1b, 0x64, 0x31};
    public static final byte[] CutFullFeed      = new byte[] {0x1b, 0x64, 0x32};
    public static final byte[] CutPartialFeed   = new byte[] {0x1b, 0x64, 0x33};
}
