package com.grubhub.android.printing;

import android.content.res.Resources;
import android.graphics.Bitmap;

import java.util.List;

/**
 * Each printer implementation to convert the
 */
public interface PrintCommandConverter {
    void convert(List<Byte> cmds, PrintStyle style);

    void convert(List<Byte> cmds, String text);

    void convert(List<Byte> cmds, Bitmap bitmap);

    void convert(List<Byte> cmds, Resources res);
}
