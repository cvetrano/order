package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.grubhub.android.orderhub.util.ViewUtils;

public class GrubHubRelativeLayout extends RelativeLayout{
    public GrubHubRelativeLayout(Context context) {
        super(context);
        ViewUtils.fixBackgroundRepeat(this);
    }

    public GrubHubRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        ViewUtils.fixBackgroundRepeat(this);
    }

    public GrubHubRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        ViewUtils.fixBackgroundRepeat(this);
    }
}
