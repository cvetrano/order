package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

public class PrefJs extends JsInterfaceBase  {
    private SharedPreferences sharedPref;

    public PrefJs(Activity activity, WebView webView){
        super(activity, webView);
        sharedPref = activity.getPreferences(
                Context.MODE_PRIVATE
        );
    }

    @JavascriptInterface
    public int getLength() {
        return sharedPref.getAll().size();
    }

    @JavascriptInterface
    public void clear() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();
    }

    @JavascriptInterface
    public String getItem(String keyName) {
        return sharedPref.getString(keyName, null);
    }

    @JavascriptInterface
    public String key(int index) {
        throw new Error("method key not implemented on PrefJs");
    }

    @JavascriptInterface
    public void removeItem(String keyName) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(keyName);
        editor.commit();
    }

    @JavascriptInterface
    public void setItem(String keyName, String keyValue) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(keyName, keyValue);
        editor.commit();
    }
}
