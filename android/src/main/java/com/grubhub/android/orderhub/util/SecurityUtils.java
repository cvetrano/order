package com.grubhub.android.orderhub.util;


import com.google.inject.Singleton;

import android.content.Context;
import android.provider.Settings;
import android.util.Base64;

@Singleton
public class SecurityUtils {

    public SecurityUtils() {}

    public byte[] createSalt(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID).getBytes();
    }

    public String base64EncodeToString(byte[] input, int flags) {
        return Base64.encodeToString(input, flags);
    }

    public byte[] base64Decode(String input, int flags) {
        return Base64.decode(input, Base64.NO_PADDING | Base64.NO_WRAP);
    }
}
