package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckBox;
import com.grubhub.android.orderhub.R;

/**
 * jsendelbach 3/18/12
 */
public class GrubhubCheckBox extends CheckBox {
    private OrderHubFont font;

    public GrubhubCheckBox(final Context context) {
        super(context);
        configureFont(context);
    }

    public GrubhubCheckBox(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    public GrubhubCheckBox(final Context context, final AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    final void configureFont(final Context context) {
        if (font == null) {
            font = OrderHubFont.defaultFont();
        }
        setTypeface(font.getTypeFace(context));
    }

    final void readAttributes(final Context context, final AttributeSet attributeSet) {
        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.GrubhubTextView);
        final int index = typedArray.getInt(R.styleable.GrubhubTextView_font, OrderHubFont.DEFAULT_FONT_INDEX);
        font = OrderHubFont.fromCode(index);
        typedArray.recycle();
    }
}
