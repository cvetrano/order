package com.grubhub.android.orderhub;

import com.google.inject.AbstractModule;

import roboguice.inject.SharedPreferencesName;

//used in androidmanifest
public class OHModule extends AbstractModule {

    protected void configure() {

        bindConstant().annotatedWith(SharedPreferencesName.class).to("com.grubhub.android.orderhub.OrderHub");

        bind(OHApplication.class).toProvider(OHApplication.OHApplicationProvider.class);
    }
}
