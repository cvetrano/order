package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.content.Context;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.util.PreferenceType;

public class DeviceInfoJs extends JsInterfaceBase {
    private DeviceUtils deviceUtils;

    public DeviceInfoJs(Activity activity, WebView webView){
        super(activity, webView);
        deviceUtils = getInjectable(DeviceUtils.class);
    }

    @JavascriptInterface
    public void markDeviceMigrated(){
        localSettings.setBoolean(PreferenceType.HasMigrated, true);
    }

    @JavascriptInterface
    public String getMacAddress() {
        return deviceUtils.getMacAddress(activity);
    }

    @JavascriptInterface
    public boolean isCharging(){
        return deviceUtils.isCharging(activity);
    }

    @JavascriptInterface
    public int getBatteryLevel(){
        return deviceUtils.getBatteryLevel(activity);
    }

    @JavascriptInterface
    public boolean isScreenOn(){
        return deviceUtils.isScreenOn(activity);
    }

    @JavascriptInterface
    public boolean isGCOpen(){
        return deviceUtils.isAppOpen();
    }
}
