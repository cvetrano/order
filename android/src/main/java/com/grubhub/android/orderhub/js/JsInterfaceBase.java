package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.webkit.WebView;

import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.util.LocalSettings;

import org.json.JSONException;
import org.json.JSONObject;
import org.roboguice.shaded.goole.common.annotations.VisibleForTesting;

import java.util.UUID;

import roboguice.RoboGuice;
import roboguice.util.Ln;

public abstract class JsInterfaceBase {
    private final WebView webView;

    protected final Activity activity;
    protected TaskFactory taskFactory;
    protected final LocalSettings localSettings;

    public JsInterfaceBase(Activity activity, WebView webView){
        this.activity = activity;
        this.webView = webView;
        taskFactory = getInjectable(TaskFactory.class);
        localSettings = getInjectable(LocalSettings.class);
    }

    protected <T> T getInjectable(Class<T> clazz){
        return RoboGuice.getInjector(activity).getInstance(clazz);
    }

    protected String generateAsyncId(){
        return UUID.randomUUID().toString();
    }

    protected JSONObject getBaseResponse(boolean successful) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("success", successful);
        }
        catch(JSONException e){
            Ln.e(e, "Error getting base JSON response. Should never happen.");
        }
        return obj;
    }

    @VisibleForTesting
    protected String getAsyncResponseString(String id, JSONObject json){
        return String.format("javascript:gcInABoxCallbacks.trigger('%s', '%s')", id, json.toString());
    }

    protected void sendAsyncResponse(String id, JSONObject json){
        final String response = getAsyncResponseString(id, json);
        webView.post(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl(response);
            }
        });
    }
}
