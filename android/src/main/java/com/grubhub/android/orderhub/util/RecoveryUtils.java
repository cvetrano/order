package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.inject.Singleton;

@Singleton
public class RecoveryUtils {
    public UpdateContext getRecoveryUpdateContext(Context context){
        UpdateContext updateContext;
        int currentVersionCode;
        try {
            currentVersionCode = context.getPackageManager().getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0).versionCode;
            updateContext = UpdateContext.RECOVERY_OLD_PACKAGE;
        }
        catch(PackageManager.NameNotFoundException e){
            try{
                //just in case they uninstalled recovery accidentally somehow, check for the settings app
                context.getPackageManager().getPackageInfo("com.grubhub.android.orderhub.settings.manager", 0);
                updateContext = UpdateContext.RECOVERY_OLD_PACKAGE;
                currentVersionCode = 0;
            }
            catch (PackageManager.NameNotFoundException e2){
                updateContext = UpdateContext.RECOVERY;
                try {
                    currentVersionCode = context.getPackageManager().getPackageInfo(UpdateContext.RECOVERY.getPackageName(), 0).versionCode;
                }
                catch(PackageManager.NameNotFoundException e3){
                    currentVersionCode = 0;
                }
            }
        }

        return currentVersionCode < 600000000 ? updateContext : null; //first self updating recovery app. otherwise recovery can update itself
    }
}
