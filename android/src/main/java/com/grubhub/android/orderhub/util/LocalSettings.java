package com.grubhub.android.orderhub.util;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import roboguice.util.Ln;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

/**
 * A wrapper around the android ShardPreferences enumerating the possible values and acquire certain
 * system/environment values particular to the device.
 *
 * Most values are retrievable only as strings, but Long and Int methods are available for convenience.
 *
 * Initially, only the value for PreferenceType.Password is encrypt
 *
 */
@Singleton
public class LocalSettings {
    public static final long DEFAULT_LONG = -1L;
    public static final int DEFAULT_INT = -1;
    public static final Boolean DEFAULT_BOOLEAN = Boolean.FALSE;

    public static final ImmutableSet<PreferenceType> LONG_PREFS = ImmutableSet.of();
    public static final ImmutableSet<PreferenceType> INT_PREFS = ImmutableSet.of();
    public static final ImmutableSet<PreferenceType> BOOLEAN_PREFS = ImmutableSet.of(
            PreferenceType.HasMigrated, PreferenceType.RecoveryUpdateTriedLastSession);

    Context context;
    SharedPreferences sharedPreferences;
    SecurityUtils securityUtils;

    private static byte[] sKey;

    OrderHubEnvironment currEnv = null;

    @Inject
    public LocalSettings(Context context, SharedPreferences sharedPreferences, SecurityUtils securityUtils) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
        this.securityUtils = securityUtils;

        initSecurityKey(context);

        // use the env saved in shared preferences, if any
        String curEnvName = getString(PreferenceType.Environment);

        if (curEnvName == null) {
            setEnvironment(OrderHubEnvironment.production);
        }
        else {
            try {
                currEnv = OrderHubEnvironment.valueOf(curEnvName);
            }
            catch (IllegalArgumentException e) {
                setEnvironment(OrderHubEnvironment.production);
            }

            if (currEnv.isCustom()) {
                currEnv.updateCustomUrls(getString(PreferenceType.CustomGrubcentralUrl),
                                         getString(PreferenceType.CustomRftHost),
                                         getString(PreferenceType.CustomUpdateBucketUrl));
            }
        }
    }

    public boolean contains(PreferenceType preferenceType) {
        return preferenceType != null && sharedPreferences.contains(preferenceType.toString());
    }

    @VisibleForTesting
    void initSecurityKey(Context context) {
        // Initialize encryption/decryption key
        try {
            final String key = generateAesKeyName(context);
            String value = sharedPreferences.getString(key, null);
            if (value == null) {
                value = generateAesKeyValue();
                sharedPreferences.edit().putString(key, value).commit();
            }
            LocalSettings.sKey = decode(value);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

    }

    @VisibleForTesting
    String encode(byte[] input)
    {
//        return Base64.encodeToString(input, Base64.NO_PADDING | Base64.NO_WRAP);
        return securityUtils.base64EncodeToString(input, Base64.NO_PADDING | Base64.NO_WRAP);
    }

    @VisibleForTesting
    private byte[] decode(String input)
    {
//        return Base64.decode(input, Base64.NO_PADDING | Base64.NO_WRAP);
        return securityUtils.base64Decode(input, Base64.NO_PADDING | Base64.NO_WRAP);
    }

    @VisibleForTesting
    String generateAesKeyName(Context context) throws InvalidKeySpecException,
            NoSuchAlgorithmException {
        final char[] password = context.getPackageName().toCharArray();

        final byte[] salt = securityUtils.createSalt(context);

        // Number of PBKDF2 hardening rounds to use, larger values increase
        // computation time, you should select a value that causes
        // computation to take >100ms
        final int iterations = 1000;

        // Generate a 256-bit key
        final int keyLength = 256;

        final KeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
        return encode(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
                .generateSecret(spec).getEncoded());
    }

    @VisibleForTesting
    String generateAesKeyValue() throws NoSuchAlgorithmException {
        // Do *not* seed secureRandom! Automatically seeded from system entropy
        final SecureRandom random = new SecureRandom();

        // Use the largest AES key length which is supported by the OS
        final KeyGenerator generator = KeyGenerator.getInstance("AES");
        try {
            generator.init(256, random);
        } catch (Exception e) {
            try {
                generator.init(192, random);
            } catch (Exception e1) {
                generator.init(128, random);
            }
        }
        return encode(generator.generateKey().getEncoded());
    }

    @VisibleForTesting
    String encrypt(String cleartext) {
        if (cleartext == null || cleartext.length() == 0) {
            return cleartext;
        }
        try {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(LocalSettings.sKey, "AES"));
            return encode(cipher.doFinal(cleartext.getBytes("UTF-8")));
        } catch (Exception e) {
            Ln.w("encrypt", e);
            return null;
        }
    }

    @VisibleForTesting
    String decrypt(String ciphertext) {
        if (ciphertext == null || ciphertext.length() == 0) {
            return ciphertext;
        }
        try {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(LocalSettings.sKey, "AES"));
            return new String(cipher.doFinal(decode(ciphertext)), "UTF-8");
        } catch (Exception e) {
            Ln.w("decrypt", e);
            return null;
        }
    }

    public String getString(PreferenceType key) {
        return getString(key, null);
    }

    public String getString(PreferenceType key, String defaultValue) {
        return sharedPreferences.getString(key.toString(), defaultValue);
    }

    public Long getLong(PreferenceType key) {
        return getLong(key, LocalSettings.DEFAULT_LONG);
    }

    public Long getLong(PreferenceType key, long defaultValue) {
        if (LocalSettings.LONG_PREFS.contains(key)) {
            return sharedPreferences.getLong(key.toString(), defaultValue);
        }

        return defaultValue;
    }

    public int getInt(PreferenceType key) {
        return getInt(key, LocalSettings.DEFAULT_INT);
    }

    public int getInt(PreferenceType key, int defaultValue) {
        if (LocalSettings.INT_PREFS.contains(key)) {
            return sharedPreferences.getInt(key.toString(), defaultValue);
        }

        return defaultValue;
    }

    public Boolean getBoolean(PreferenceType key) {
        return getBoolean(key, LocalSettings.DEFAULT_BOOLEAN);
    }


    public Boolean getBoolean(PreferenceType key, boolean defaultValue) {
        if (LocalSettings.BOOLEAN_PREFS.contains(key)) {
            return sharedPreferences.getBoolean(key.toString(), defaultValue);
        }

        return defaultValue;
    }

    public void set(PreferenceType key, String value) {
        sharedPreferences.edit().putString(key.toString(), value).commit();
    }

    public void setLong(PreferenceType key, Long value) {
        sharedPreferences.edit().putLong(key.toString(), (value == null ? LocalSettings.DEFAULT_LONG : value)).commit();
    }

    public void setInt(PreferenceType key, int value) {
        sharedPreferences.edit().putInt(key.toString(), value).commit();
    }

    public void setBoolean(PreferenceType key, boolean value) {
        sharedPreferences.edit().putBoolean(key.toString(), value).commit();
    }

    public void setEnvironment(OrderHubEnvironment newEnv) {
        currEnv = newEnv;
        set(PreferenceType.Environment, currEnv.name());
        if(currEnv.isCustom()){
            set(PreferenceType.CustomGrubcentralUrl, newEnv.getGrubcentralUrl());
            set(PreferenceType.CustomRftHost, newEnv.getRftHost());
            set(PreferenceType.CustomUpdateBucketUrl, newEnv.getUpdateBucketUrl());
        }
    }

    public OrderHubEnvironment getCurrentEnvironment() {
        return currEnv;
    }

    /**
     * Clear all or a subset of preference types.
     *
     * @param preferenceTypes preferenceTypes to remove - if null, all preferences will be removed
     */
    public void clearPreferences(PreferenceType[] preferenceTypes) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if ((preferenceTypes == null) || (preferenceTypes.length == 0)) {
            editor.clear();
        } else {
            for (PreferenceType pref : preferenceTypes) {
                editor.remove(pref.toString());
            }
        }

        editor.commit();
    }

    /**
     * Clear a specific preference type.
     *
     * @param preferenceType preferenceType to remove - if null, won't remove anything
     */
    public void clearPreference(PreferenceType preferenceType) {
        if (preferenceType != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(preferenceType.toString());
            editor.commit();
        }
    }

}
