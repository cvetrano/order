package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import roboguice.util.Ln;

/**
 * jsendelbach 3/18/12
 */
public enum OrderHubFont {
    GRUBHUB("GrubHubFont.ttf", 0),
    NORMAL("RegularNormal.ttf", 1),
    BOLD("RegularBold.ttf", 2),
    ITALIC("RegularItalic.ttf", 3),
    CONDENSED_NORMAL("CondensedNormal.ttf", 4),
    CONDENSED_BOLD("CondensedBold.ttf", 5),
    CONDENSED_ITALIC("CondensedItalic.ttf", 6);

    private final String fileName;
    private final int code;
    private Typeface typeface;

    private OrderHubFont(final String fileName, final int code) {
        this.fileName = fileName;
        this.code = code;
    }

    public static OrderHubFont fromCode(final int code) {
        for (OrderHubFont font : values()) {
            if (font.code == code) {
                return font;
            }
        }
        return null;
    }

    public Typeface getTypeFace(final Context context) {
        if (typeface == null) {
            final AssetManager assets = context.getAssets();
            try {
                typeface = Typeface.createFromAsset(assets, fileName);
            } catch (Exception e) {
                Ln.e(e, "Could not load font" + this);
            }
        }
        return typeface;
    }

    public static OrderHubFont defaultFont() {
        return NORMAL;
    }

    public static final int DEFAULT_FONT_INDEX = 1;
}
