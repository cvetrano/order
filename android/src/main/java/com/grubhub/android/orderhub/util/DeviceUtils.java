package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.PowerManager;

import com.google.inject.Singleton;

import roboguice.util.Ln;

//TODO: move this to the js file when we no longer need it for migration checks
@Singleton
public class DeviceUtils {
    static final IntentFilter BATTERY_CHANGED_INTENT_FILTER = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    private static boolean isActive = false;
    public String getMacAddress(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        if (manager == null) {
            Ln.e("couldn't get the WifiManager service");
            return null;
        }

        WifiInfo info = manager.getConnectionInfo();

        if (info == null) {
            Ln.e("couldn't get the WifiInfo from the WifiManager");
            return null;
        }

        String macAddress = info.getMacAddress();

        if (macAddress == null) {
            Ln.e("MAC address is null? I guess the network is disabled :(");
        }

        return macAddress;
    }

    public boolean isCharging(Context context){
        Intent batteryIntent = context.registerReceiver(null, BATTERY_CHANGED_INTENT_FILTER);
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        return status == BatteryManager.BATTERY_STATUS_CHARGING;
    }

    public int getBatteryLevel(Context context){
        Intent batteryIntent = context.registerReceiver(null, BATTERY_CHANGED_INTENT_FILTER);
        return batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }

    public boolean isScreenOn(Context context){
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        return powerManager.isScreenOn();
    }

    public boolean isAppOpen(){
        return isActive;
    }

    public static void setIsActive(boolean isActive){
        DeviceUtils.isActive = isActive;
    }
}
