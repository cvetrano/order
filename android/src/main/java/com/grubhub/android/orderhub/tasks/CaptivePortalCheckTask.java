package com.grubhub.android.orderhub.tasks;

import android.content.Context;
import android.os.Build;

import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;

import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import roboguice.util.Ln;

public class CaptivePortalCheckTask extends AbstractOrderHubTask<Void, Void, Boolean> {
    private TaskResponseHandler<Boolean> handler;

    public CaptivePortalCheckTask(final Context context, TaskResponseHandler<Boolean> handler){
        super(context);
        this.handler = handler;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("http://clients3.google.com/generate_204");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(5000);
            urlConnection.setUseCaches(false);
            urlConnection.getInputStream();
            // We got a valid response, but not from the real google
            return urlConnection.getResponseCode() != 204;
        }
        catch (IOException e) {
            Ln.e(e, "Walled garden check - probably not a portal");
            return false;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean isCaptivePortal) {
        handler.handleResponse(isCaptivePortal);
    }

}
