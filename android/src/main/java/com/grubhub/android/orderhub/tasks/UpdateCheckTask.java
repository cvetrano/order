package com.grubhub.android.orderhub.tasks;

import android.content.Context;
import android.os.Build;

import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;

import org.joda.time.DateTime;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import roboguice.util.Ln;

public class UpdateCheckTask extends AbstractOrderHubTask<Void, Void, Boolean> {
    private TaskResponseHandler<Boolean> handler;

    public UpdateCheckTask(final Context context, TaskResponseHandler<Boolean> handler){
        super(context);
        this.handler = handler;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        if(BuildConfig.DEBUG && !Build.FINGERPRINT.equals("robolectric")){ //don't want to skip for unit tests
            Ln.i("Skipping update check for local build");
            return false;
        }

        localSettings.setLong(PreferenceType.LastUpdateCheck, new DateTime().toDate().getTime());

        BufferedReader in = null;
        StringBuilder responseBuilder = new StringBuilder();
        OrderHubEnvironment env = localSettings.getCurrentEnvironment();
        String updateUrl = env.getUpdateBucketUrl() + "gcinabox.versioncode";
        try {
            URL url = new URL(updateUrl);
            URLConnection conn = url.openConnection();
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;

            while ((line = in.readLine()) != null) {
                responseBuilder.append(line);
            }

            int currentVersionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode,
                newVersionCode = Integer.parseInt(responseBuilder.toString());

            if(newVersionCode > currentVersionCode){
                Ln.i("Update required from %d from %d", currentVersionCode, newVersionCode);
                return true;
            }
            else{
                Ln.i("Update check said no update. current version code: %d. new version code: %d", currentVersionCode, newVersionCode);
            }
        }
        catch(NumberFormatException e){
            Ln.e("Error parsing new version code %s from update check url", responseBuilder.toString());
        }
        catch(Exception e){
            Ln.e("Error checking for updates from %s", updateUrl);
        }
        finally{
            if(in != null){
                try{
                    in.close();
                }
                catch(Exception e){
                    Ln.w("error closing buffered reader after update check");
                }
            }
        }
        return false;
    }

    @Override
    protected void onPostExecute(Boolean updateNeeded) {
        handler.handleResponse(updateNeeded);
    }

}
