package com.grubhub.android.orderhub.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.dialogs.SecretSettingsCustomUrlDialog;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.LocalSettings;

import roboguice.inject.InjectView;
import roboguice.util.Ln;

public class SecretSettingsActivity extends OrderHubActivity {

    @Inject
    LocalSettings localSettings;
    @Inject
    SecretSettingsCustomUrlDialog secretSettingsCustomUrlDialog;

    @InjectView(R.id.secret_settings_close)
    Button closeButton;
    @InjectView(R.id.secret_settings_gc_url_text)
    TextView gcUrl;
    @InjectView(R.id.secret_settings_rft_host_text)
    TextView rftHost;
    @InjectView(R.id.secret_settings_update_bucket_url_text)
    TextView updateBucketUrl;
    @InjectView(R.id.secret_settings_env_choose_from_list)
    Button chooseEnvFromList;
    @InjectView(R.id.secret_settings_clear_settings)
    Button clearPreferences;
    @InjectView(R.id.secret_settings_clear_migration_flag)
    Button clearMigrationFlag;
    @InjectView(R.id.secret_settings_set_migration_flag)
    Button setMigrationFlag;
    @InjectView(R.id.secret_settings_go_to_js_test_page)
    Button jsTestPageBtn;
    @InjectView(R.id.secret_settings_force_crash)
    Button forceCrashButton;

    OrderHubEnvironment currentEnv;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secret_settings);

        currentEnv = localSettings.getCurrentEnvironment();
        updateUrlDisplays(currentEnv);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                startActivity(new Intent(SecretSettingsActivity.this, WelcomeActivity.class));
                finish();
            }
        });
        jsTestPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SecretSettingsActivity.this, JsTestActivity.class));
                finish();
            }
        });
        chooseEnvFromList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                changeEnv();
            }
        });
        clearPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                localSettings.clearPreferences(null);
            }
        });
        clearMigrationFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                localSettings.clearPreference(PreferenceType.HasMigrated);
            }
        });
        setMigrationFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                localSettings.setBoolean(PreferenceType.HasMigrated, true);
            }
        });
        forceCrashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                throw new RuntimeException("This is a test crash");
            }
        });
    }

    private void changeEnv() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose Environment");

        final OrderHubEnvironment[] envList = OrderHubEnvironment.values();
        String[] envNames = new String[envList.length];
        for (int i = 0; i < envList.length; i++) {
            envNames[i] = envList[i].name();
        }

        builder.setItems(envNames, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                OrderHubEnvironment selectedEnv = envList[which];
                updateEnv(selectedEnv);

                if (selectedEnv.isCustom()) {
                    getCustomUrls(selectedEnv, currentEnv);
                }
                currentEnv = selectedEnv;
            }
        });
        builder.show();
    }

    private void updateUrlDisplays(OrderHubEnvironment env){
        gcUrl.setText(env.getGrubcentralUrl());
        rftHost.setText(env.getRftHost());
        updateBucketUrl.setText(env.getUpdateBucketUrl());
    }

    private void updateEnv(OrderHubEnvironment env) {
        updateUrlDisplays(env);
        localSettings.setEnvironment(env);
    }

    private void getCustomUrls(final OrderHubEnvironment selectedEnv, final OrderHubEnvironment previousEnv) {
        SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler successHandler = new SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler() {
            @Override
            public void dialogFinished(String gcUrl, String rftHost, String updateBucketUrl) {
                selectedEnv.updateCustomUrls(gcUrl, rftHost, updateBucketUrl);
                updateEnv(selectedEnv);
            }
        };
        Runnable failHanddler = new Runnable() {
            @Override
            public void run() {
                updateEnv(previousEnv);
                currentEnv = previousEnv;
            }
        };

        try {
            secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(selectedEnv, successHandler, failHanddler);
        }
        catch(SecretSettingsCustomUrlDialog.InvalidCustomEnvironmentException e){
            Ln.e("Can't edit non-custom environment");
        }
    }

    public static AlertDialog.Builder createSecretSettingsDialogBuilder(LayoutInflater layoutInflater, Context context, String headerString) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final View title = layoutInflater.inflate(R.layout.secret_settings_dialog_header, null);
        final TextView header = (TextView) title.findViewById(R.id.header_text);

        header.setText(headerString);

        builder.setCustomTitle(title);
        return builder;
    }
}
