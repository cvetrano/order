package com.grubhub.android.orderhub.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.grubhub.android.orderhub.util.LocalSettings;

import roboguice.RoboGuice;

public abstract class AbstractOrderHubTask<PARAMS, PROGRESS, RESULT> extends AsyncTask<PARAMS, PROGRESS, RESULT>{
    protected final Context context;
    protected final LocalSettings localSettings;

    public AbstractOrderHubTask(final Context context){
        this.context = context;
        localSettings = RoboGuice.getInjector(context).getInstance(LocalSettings.class);
    }
}
