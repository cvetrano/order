package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.grubhub.android.orderhub.util.ViewUtils;

public class GrubHubLinearLayout extends LinearLayout{
    public GrubHubLinearLayout(final Context context) {
        super(context);
        ViewUtils.fixBackgroundRepeat(this);
    }

    public GrubHubLinearLayout(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
        ViewUtils.fixBackgroundRepeat(this);
    }
}
