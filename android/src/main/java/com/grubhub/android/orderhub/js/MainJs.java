package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;


public class MainJs extends JsInterfaceBase {
    public MainJs(Activity activity, WebView webView){
        super(activity, webView);
    }

    @JavascriptInterface
    public String hello(){
        return "react + avalon";
    }

    @JavascriptInterface
    public String getVersionString(){
        try {
            PackageInfo pi = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            return pi.versionName;
        }
        catch(PackageManager.NameNotFoundException e){ //should never happen
            return null;
        }
    }

    @JavascriptInterface
    public int getVersionCode(){
        try {
            PackageInfo pi = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            return pi.versionCode;
        }
        catch(PackageManager.NameNotFoundException e){ //should never happen
            return 0;
        }
    }

    @JavascriptInterface
    public void reloadPage(){
        activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.recreate();
                }
            });
    }

}
