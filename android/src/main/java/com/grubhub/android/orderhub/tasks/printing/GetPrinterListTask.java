package com.grubhub.android.orderhub.tasks.printing;

import android.content.Context;

import com.grubhub.android.orderhub.tasks.AbstractOrderHubTask;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PrintUtils;
import com.grubhub.android.printing.Printer;

import java.util.List;

import roboguice.RoboGuice;

public class GetPrinterListTask extends AbstractOrderHubTask<Void, Void, List<Printer>> {
    private TaskResponseHandler<List<Printer>> handler;
    private PrintUtils printUtils;

    public GetPrinterListTask(Context context, TaskResponseHandler<List<Printer>> handler){
        super(context);
        this.handler = handler;
        printUtils = RoboGuice.getInjector(context).getInstance(PrintUtils.class);
    }

    @Override
    protected List<Printer> doInBackground(Void... voids) {
        return printUtils.getAvailablePrinters();
    }

    @Override
    protected void onPostExecute(List<Printer> printers) {
        super.onPostExecute(printers);
        handler.handleResponse(printers);
    }
}
