package com.grubhub.android.orderhub.widget;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;

import roboguice.RoboGuice;

/**
 * A simple extension of the android Dialog class that sets and resets the latest dialog on show() and hide() respectively
 * with DialogTracker
 */
public class OrderHubDialog extends Dialog {
    public OrderHubDialog(Context context) {
        super(context);
        RoboGuice.getInjector(context).injectMembers(this);
    }

    public OrderHubDialog(Context context, int theme) {
        super(context, theme);
        RoboGuice.getInjector(context).injectMembers(this);
    }

    protected OrderHubDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        RoboGuice.getInjector(context).injectMembers(this);
    }

    @Override
    public void show() {
        if (!activityIsFinishing()) {
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            super.show();
        }
    }

    // See this: http://dimitar.me/android-displaying-dialogs-from-background-threads/
    // Basically a crash can occur if you try to show this dialog as the associated activity is
    // shutting down (ideally the asynchronous context from which this class is called would not
    // be triggered on shutdown, but it is)
    private boolean activityIsFinishing() {
        final Context context = getContext();
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            return activity.isFinishing();
        } else {
            return false; // this really should be a context/not null, but let's assume this condition means that the activity isn't finishing
        }
    }
}
