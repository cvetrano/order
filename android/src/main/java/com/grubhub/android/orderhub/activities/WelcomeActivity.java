package com.grubhub.android.orderhub.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.http.SslError;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TableLayout;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.dialogs.WifiAuthDialog;
import com.grubhub.android.orderhub.js.DeviceInfoJs;
import com.grubhub.android.orderhub.js.MainJs;
import com.grubhub.android.orderhub.js.PrefJs;
import com.grubhub.android.orderhub.js.PrintJs;
import com.grubhub.android.orderhub.js.UpdateJs;
import com.grubhub.android.orderhub.services.UpdateService;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.RecoveryUtils;

import java.util.Date;

import roboguice.inject.InjectView;
import roboguice.util.Ln;
import com.grubhub.android.orderhub.util.PermissionUtil;
import android.Manifest;
import android.util.Log;
import android.support.v4.app.ActivityCompat;
import android.content.pm.PackageManager;

public class WelcomeActivity extends OrderHubActivity {
    private static final String TAG = "WelcomeActivity";

    @InjectView(R.id.mainWebView)
    protected FrameLayout mainWebViewPlaceholder;
    @InjectView(R.id.loaderWebview)
    protected FrameLayout loaderWebViewPlaceholder;

    WebView mainWebView;
    WebView loaderWebView;
    boolean pageLoaded = false;
    Date lastClosed = null;

    private static final String LOADING_FAILED_URL = "file:///android_asset/loading_failed.html";

    @Inject
    LayoutInflater layoutInflater;
    @Inject
    LocalSettings localSettings;
    @Inject
    RecoveryUtils recoveryUtils;
    @Inject
    WifiAuthDialog wifiAuthDialog;
    @Inject
    PermissionUtil permissionUtil;

    @InjectView(R.id.secret_settings_open_button)
    View secretSettingsButton;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableRemoteDebuggingIfDebugOrNotProd();
        setContentView(R.layout.web_view);
        initLoaderWebView();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(networkReceiver, filter);

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(UpdateService.FULL_UPDATE_CHECK_INTENT_ACTION));

        secretSettingsButton.setOnTouchListener(secretSettingsButtonTouchListener);

        checkAndRequestPermission();

        doMigrationCheck();
    }

    @Override
    protected void onResume(){
        super.onResume();
        DeviceUtils.setIsActive(true);
        if(lastClosed != null){
            Date now = new Date();
            if(now.getTime() - lastClosed.getTime() > 24*60*60*1000){ //1 day
                finish(); //restart activity if it was closed for more than an hour
            }
            lastClosed = null;
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        lastClosed = new Date();
        DeviceUtils.setIsActive(false);
    }

    private void checkAndRequestPermission() {
        if (permissionUtil.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.d(TAG, "Already have WRITE_EXTERNAL_STORAGE permission on launch");
        }
        else {
            Log.d(TAG, "Does not have WRITE_EXTERNAL_STORAGE permission on launch, requesting");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
    }

    private void enableRemoteDebuggingIfDebugOrNotProd(){
        if(Build.VERSION.SDK_INT < 19){ //this isn't supported at the minimum api level
            return;
        }

        WebView.setWebContentsDebuggingEnabled(BuildConfig.DEBUG || localSettings.getCurrentEnvironment() != OrderHubEnvironment.production);
    }

    private void doMigrationCheck(){
        taskFactory.makeMigrationCheckTask(WelcomeActivity.this, new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                initWebView(result);
            }
        }).execute();
    }

    private void initLoaderWebView(){
        if(loaderWebView == null){
            loaderWebView = new WebView(this);
            loaderWebView.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
            loaderWebView.getSettings().setLoadsImagesAutomatically(true);
            loaderWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            loaderWebView.loadUrl("file:///android_asset/loader.html");
            loaderWebViewPlaceholder.addView(loaderWebView);
        }
    }

    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(mainWebView != null && mainWebView.getUrl().equals(LOADING_FAILED_URL)){
                WelcomeActivity.this.recreate();
            }
        }
    };

    private String wifiAuthReloadUrl;
    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView(boolean goToMigration) {
        if (mainWebView == null) {
            // Create the webview
            mainWebView = new WebView(this);
            mainWebView.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
            mainWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            mainWebView.setScrollbarFadingEnabled(true);
            mainWebView.getSettings().setSupportZoom(false);
            mainWebView.getSettings().setBuiltInZoomControls(false);
            mainWebView.getSettings().setLoadsImagesAutomatically(true);
            mainWebView.getSettings().setJavaScriptEnabled(true);
            mainWebView.getSettings().setAllowFileAccess(false);
            mainWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            mainWebView.getSettings().setDomStorageEnabled(true);
            mainWebView.getSettings().setDatabaseEnabled(true);
            mainWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);

            // Attach the WebView to its placeholder
            mainWebViewPlaceholder.addView(mainWebView);
            mainWebView.addJavascriptInterface(new MainJs(this, mainWebView), "gcInABox");
            mainWebView.addJavascriptInterface(new PrintJs(this, mainWebView), "gcInABoxPrinting");
            mainWebView.addJavascriptInterface(new DeviceInfoJs(this, mainWebView), "gcInABoxDevice");
            mainWebView.addJavascriptInterface(new UpdateJs(this, mainWebView), "gcInABoxUpdate");
            mainWebView.addJavascriptInterface(new PrefJs(this, mainWebView), "gcInABoxPrefs");

            OrderHubEnvironment env = localSettings.getCurrentEnvironment();
            if (goToMigration) {
                wifiAuthReloadUrl = env.getMigrationUrl();
                mainWebView.loadUrl(wifiAuthReloadUrl);
            } else {
                wifiAuthReloadUrl = env.getGrubcentralUrl();
                boolean recoveryUpdateTriedLastSession = localSettings.getBoolean(PreferenceType.RecoveryUpdateTriedLastSession, false);
                if(recoveryUpdateTriedLastSession && recoveryUtils.getRecoveryUpdateContext(this) != null){
                    wifiAuthReloadUrl += "?recoveryUpdateFailed";
                }
                if(recoveryUpdateTriedLastSession){
                    localSettings.clearPreference(PreferenceType.RecoveryUpdateTriedLastSession);
                }
                mainWebView.loadUrl(wifiAuthReloadUrl);
            }

        }

        mainWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                taskFactory.makeCaptivePortalCheck(WelcomeActivity.this, new TaskResponseHandler<Boolean>() {
                    @Override
                    public void handleResponse(Boolean result) {
                        if(result) {
                            displayWifiAuthDialog();
                        }
                    }
                }).execute();

                Ln.w(error);
                handler.proceed();
            }

            //not sure why the non-deprecated one is not getting called. try later
            @Override
            @SuppressLint("deprecated")
            public void onReceivedError(WebView webView, int errorCode, String description, String failingUrl){
                Ln.e("Error opening webview: %d %s.", errorCode, description);
                taskFactory.makeCaptivePortalCheck(WelcomeActivity.this, new TaskResponseHandler<Boolean>() {
                    @Override
                    public void handleResponse(Boolean result) {
                        if(result) {
                            Ln.i("Captive portal check task returned true. Opening wifi auth dialog.");
                            displayWifiAuthDialog();
                        }
                        else{
                            Ln.i("Captive portal check task returned false. Opening network error page.");
                            mainWebView.loadUrl(LOADING_FAILED_URL);
                        }
                    }
                }).execute();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                loaderWebViewPlaceholder.setVisibility(View.INVISIBLE);
                mainWebViewPlaceholder.setVisibility(View.VISIBLE);
                pageLoaded = true;
            }
        });
    }

    private void displayWifiAuthDialog(){
        wifiAuthDialog.displayDialog(new Runnable() {
            @Override
            public void run() {
                mainWebView.loadUrl(wifiAuthReloadUrl != null ? wifiAuthReloadUrl : mainWebView.getUrl());
            }
        });
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(networkReceiver);
        if (mainWebView != null) {
            mainWebView.destroy();
        }
        if (loaderWebView != null) {
            loaderWebView.destroy();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mainWebView != null) {
            // Save the state of the WebView
            mainWebView.saveState(outState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(mainWebView != null) {
            // Restore the state of the WebView
            mainWebView.restoreState(savedInstanceState);
        }
    }

    @Override
    public void onBackPressed() {
        if (mainWebView != null) {
            if (mainWebView.canGoBack()) {
                mainWebView.goBack();
            }
        }
        else {
            super.onBackPressed();
        }
    }

    private static final String SECRET_PASSWORD = "67337482";
    private void openSecretSettings(){
        OrderHubEnvironment curEnv = localSettings.getCurrentEnvironment();
        if (curEnv == OrderHubEnvironment.production) {
            final View secretSettingsView = layoutInflater.inflate(R.layout.secret_settings_dialog, null);

            SecretSettingsActivity.createSecretSettingsDialogBuilder(layoutInflater, WelcomeActivity.this, getString(R.string.secret_settings_alert_dialog_title))
                    .setCancelable(false)
                    .setView(secretSettingsView)
                    // Yes, I switched the negative and positive buttons per Zeke's mockups
                    .setPositiveButton(R.string.secret_settings_alert_dialog_negative_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                        }
                    })
                    .setNegativeButton(R.string.secret_settings_alert_dialog_positive_button_text, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            String actualPassword = ((EditText) secretSettingsView.findViewById(R.id.secret_settings_password)).getText().toString();

                            if (SECRET_PASSWORD.equals(actualPassword)) {
                                startActivity(new Intent(getBaseContext(), SecretSettingsActivity.class));
                                finish();
                            }
                        }
                    }).show();
        } else {
            startActivity(new Intent(WelcomeActivity.this, SecretSettingsActivity.class));
            finish();
        }
    }

    private int numSecretButtonClicks = 0;
    private Long secretButtonTimerBegin = 0L;
    final View.OnTouchListener secretSettingsButtonTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if(motionEvent.getAction() != MotionEvent.ACTION_DOWN) {
                return false;
            }

            Long curTime = System.currentTimeMillis();
            if(curTime - secretButtonTimerBegin < 2500L)
                numSecretButtonClicks++;
            else {
                secretButtonTimerBegin = curTime;
                numSecretButtonClicks = 1;
            }
            if(numSecretButtonClicks == 10)
            {
                openSecretSettings();
            }
            return false;
        }
    };
}
