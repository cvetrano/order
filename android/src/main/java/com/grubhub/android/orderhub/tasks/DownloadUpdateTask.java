package com.grubhub.android.orderhub.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.util.UpdateContext;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import roboguice.util.Ln;

public class DownloadUpdateTask extends AbstractOrderHubTask<Void, Integer, Boolean> {
    private static final String DOWNLOAD_DIR = "/Download/";

    private final TaskResponseHandler<Boolean> resultHandler;
    private UpdateContext updateContext;

    public DownloadUpdateTask(UpdateContext updateContext, Context context, TaskResponseHandler<Boolean> resultHandler){
        super(context);
        this.updateContext = updateContext;
        this.resultHandler = resultHandler;
        this.progressDialog = new AttachedStatusProgressDialog(context);
    }

    AttachedStatusProgressDialog progressDialog;

    @Override
    protected void onPreExecute(){
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.app_update_downloading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();

        // Check if the 'Download' directory exists. If not, create it
        File f = new File(Environment.getExternalStorageDirectory() + DOWNLOAD_DIR);
        if(!f.isDirectory() || !f.exists()) {
            Ln.w("Download directory does not exist, creating....");
            boolean result = f.mkdir();
            if (result) {
                Ln.i("Download directory created!");
            }
            else {
                Ln.e("Could not create Download directory, updates cannot be downloaded and installed!");
            }
        }


    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        // process some tasks
        int contentLength = 0;
        int completedLength = 0;
        FileOutputStream fos = null;
        InputStream in = null;
        try {
            OrderHubEnvironment env = localSettings.getCurrentEnvironment();
            URL url = new URL(env.getUpdateBucketUrl() + updateContext.getFilename());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            contentLength = conn.getContentLength();

            File outputFile = new File(Environment.getExternalStorageDirectory() + DOWNLOAD_DIR, updateContext.getFilename());
            fos = new FileOutputStream(outputFile);
            in = new BufferedInputStream(conn.getInputStream());

            byte[] buffer = new byte[8192];
            int len;
            while ((len = in.read(buffer)) != -1) {
                fos.write(buffer, 0, len);

                completedLength += len;
                // Update the progress bar
                publishProgress(completedLength * 100 / contentLength);
            }
            fos.close();
        } catch (Exception e) {
            Ln.e(e, "Update download/install error");
            return false;
        }
        finally {
            try {
                if (fos != null) {
                    fos.close();
                }
                if(in != null){
                    in.close();
                }
            }
            catch(Exception e){
                Ln.w("error closing streams after update download");
            }
        }

        return completedLength == contentLength && contentLength > 0;
    }

    @Override
    protected void onProgressUpdate(Integer... progress){
        progressDialog.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (progressDialog.isAttached()) {
            progressDialog.dismiss();
        }

        if(resultHandler != null){
            resultHandler.handleResponse(result);
        }
        if(result) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + DOWNLOAD_DIR + updateContext.getFilename()));
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
            if(updateContext != UpdateContext.SELF) {
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            context.startActivity(intent);
        }
    }

    static class AttachedStatusProgressDialog extends ProgressDialog {
        private boolean attached = false;

        public AttachedStatusProgressDialog(final Context context) {
            super(context);
        }

        @Override
        public void onAttachedToWindow() {
            attached = true;
        }

        @Override
        public void onDetachedFromWindow() {
            attached = false;
        }

        public boolean isAttached() {
            return attached;
        }
    }
}
