package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.PrintResultCode;
import com.grubhub.android.printing.Printer;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.roboguice.shaded.goole.common.annotations.VisibleForTesting;

import java.util.List;

import roboguice.util.Ln;

public class PrintJs extends JsInterfaceBase {
    private Printer currentPrinter;

    public PrintJs(Activity activity, WebView webView){
        super(activity, webView);
        currentPrinter = null;
        refreshDefaultPrinter(null);
    }

    @JavascriptInterface
    public String getAvailablePrinters(){
        final String asyncId = generateAsyncId();
        taskFactory.makePrinterListTask(activity, new TaskResponseHandler<List<Printer>>() {
            @Override
            public void handleResponse(List<Printer> result) {
                try {
                    JSONObject response = getBaseResponse(result != null && result.size() > 0);
                    if (result != null) {
                        JSONArray arr = new JSONArray();
                        for (Printer printer : result) {
                            arr.put(getPrinterJsonObj(printer));
                        }
                        response.put("printers", arr);
                    }
                    sendAsyncResponse(asyncId, response);
                }
                catch(JSONException e){
                    sendAsyncResponse(asyncId, getBaseResponse(false));
                    Ln.e(e, "Error getting printer list");
                }
            }
        }).execute();

        return asyncId;
    }

    @JavascriptInterface
    public void savePrinter(String ipAddress, String macAddress, String printerName){
        Ln.i("Save printer called: ip: %s. mac: %s. name: %s", ipAddress, macAddress, printerName);
        localSettings.set(PreferenceType.DefaultPrinterMacAddress, macAddress);
        currentPrinter = new Printer(ipAddress, macAddress, printerName);
    }

    @JavascriptInterface
    public String getCurrentPrinter() {
        Ln.i("Get current printer called");
        try {
            JSONObject printer = getPrinterJsonObj(currentPrinter);
            Ln.i("printer json: %s", printer.toString());
            return printer.toString();
        }
        catch(JSONException e){
            Ln.e(e, "Error getting printer json.");
            return new JSONObject().toString();
        }
    }

    @JavascriptInterface
    public String printImage(final String image, final int numCopies){
        Ln.i("Printing image. numCopies: %d", numCopies);
        final String asyncId = generateAsyncId();
        tryToPrintImage(asyncId, image, numCopies, true);
        return asyncId;
    }

    private JSONObject getResponseForPrintResult(PrintResult result){
        JSONObject response = getBaseResponse(result.isSuccess());
        if(!result.isSuccess()) {
            try {
                response.put("resultCode", result.getResultCode() != null ? result.getResultCode().toString() : null);
                response.put("exceptionMessage", result.getPrintExceptionMessage());
            }
            catch(JSONException e){
                Ln.e(e, "Error getting printer response");
            }
        }
        return response;
    }
    
    private void tryToPrintImage(final String asyncId, final String image, final int numCopies, final boolean resyncIfOffline){
        taskFactory.makePrintImageTask(activity, currentPrinter, image, numCopies, new TaskResponseHandler<PrintResult>() {
            @Override
            public void handleResponse(PrintResult result) {
                Ln.i("print result received: success: %s. code: %s", result.isSuccess(), result.isSuccess() ? "null" : result.getResultCode());
                if(resyncIfOffline && !result.isSuccess() && result.getResultCode().equals(PrintResultCode.PrinterOffline)){
                    resyncPrinterAndRetryPrint(asyncId, image, numCopies);
                    return;
                }

                sendAsyncResponse(asyncId, getResponseForPrintResult(result));
            }
        }).execute();
    }

    private void resyncPrinterAndRetryPrint(final String asyncId, final String image, final int numCopies){
        refreshDefaultPrinter(new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                if(!result){
                    PrintResult printResult = new PrintResult();
                    printResult.setSuccess(false);
                    printResult.setResultCode(PrintResultCode.PrinterOffline);
                    sendAsyncResponse(asyncId, getResponseForPrintResult(printResult));
                    return;
                }
                tryToPrintImage(asyncId, image, numCopies, false);
            }
        });
    }

    @VisibleForTesting
    protected JSONObject getPrinterJsonObj(Printer p) throws JSONException {
        JSONObject obj = new JSONObject();
        if(p == null){
            return obj;
        }
        obj.put("name", p.getPrinterName());
        obj.put("ipAddress", p.getIpAddress());
        obj.put("macAddress", p.getMacAddress());
        return obj;
    }

    private void refreshDefaultPrinter(final TaskResponseHandler<Boolean> foundHandler){
        Ln.i("refreshing default printer");
        final String macAddr = localSettings.getString(PreferenceType.DefaultPrinterMacAddress);
        if(StringUtils.isBlank(macAddr)){
            Ln.i("jk. not refreshing default printer");
            return;
        }
        taskFactory.makePrinterListTask(activity, new TaskResponseHandler<List<Printer>>() {
            @Override
            public void handleResponse(List<Printer> result) {
                boolean foundPrinter = false;
                if(result != null) {
                    for(Printer printer : result){
                        if(printer.getMacAddress().equals(macAddr)){
                            currentPrinter = printer;
                            foundPrinter = true;
                            break;
                        }
                    }
                }
                Ln.i("refreshing default printer finished: printer found= %s", foundPrinter ? currentPrinter.toString() : "none");
                if(foundHandler != null){
                    foundHandler.handleResponse(foundPrinter);
                }
            }
        }).execute();
    }
}
