package com.grubhub.android.orderhub.tasks.printing;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.grubhub.android.orderhub.tasks.AbstractOrderHubTask;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PrintUtils;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.PrintResultCode;
import com.grubhub.android.printing.Printer;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;

import roboguice.RoboGuice;
import roboguice.util.Ln;

public class PrintImageTask extends AbstractOrderHubTask<Void, Void, PrintResult>{
    private Printer printer;
    private String encodedImage;
    private int numCopies;
    private TaskResponseHandler<PrintResult> handler;
    private PrintUtils printUtils;

    public PrintImageTask(Context context, Printer printer, String encodedImage, int numCopies, TaskResponseHandler<PrintResult> handler) {
        super(context);
        this.printer = printer;
        this.encodedImage = encodedImage;
        this.numCopies = numCopies;
        this.handler = handler;
        printUtils = RoboGuice.getInjector(context).getInstance(PrintUtils.class);
    }

    @Override
    protected PrintResult doInBackground(Void... voids) {
        if(printer == null){
            PrintResult result = new PrintResult();
            result.setSuccess(false);
            result.setResultCode(PrintResultCode.NoPrinterFound);
            return result;
        }
        else if (StringUtils.isBlank(encodedImage) || encodedImage.equals("null") || encodedImage.equals("undefined") || encodedImage.equals("false")) {
            PrintResult result = new PrintResult();
            result.setSuccess(false);
            result.setResultCode(PrintResultCode.EmptyPrintImage);
            return result;
        }

        Bitmap printDataBitmap;
        try {
            byte[] printData = Base64.decode(encodedImage, 0);
            printDataBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(printData));
        }
        catch(Exception e){
            PrintResult result = new PrintResult();
            result.setSuccess(false);
            result.setResultCode(PrintResultCode.BadPrintImage);
            return result;
        }
        Ln.i("attempting to print bitmap");
        return printUtils.printImage(context, printer, printDataBitmap, numCopies);
    }

    @Override
    protected void onPostExecute(PrintResult printResult) {
        super.onPostExecute(printResult);
        handler.handleResponse(printResult);
    }
}
