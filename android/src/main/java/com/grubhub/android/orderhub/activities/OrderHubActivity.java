package com.grubhub.android.orderhub.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.dialogs.OHDialogBuilder;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.UpdateContext;

import roboguice.activity.RoboActivity;

import android.Manifest;
import android.util.Log;
import android.support.v4.app.ActivityCompat;
import android.content.pm.PackageManager;
import com.grubhub.android.orderhub.util.PermissionUtil;

public abstract class OrderHubActivity extends RoboActivity {

    private static final String TAG = "OrderHubActivity";

    @Inject
    TaskFactory taskFactory;
    @Inject
    PermissionUtil permissionUtil;

    @Override
    protected void onStart(){
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        for(UpdateContext updateContext : UpdateContext.values()){
            intentFilter.addAction(updateContext.getIntentActionName());
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(appUpdateListener, intentFilter);
    }


    @Override
    protected void onStop(){
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(appUpdateListener);
    }

    BroadcastReceiver appUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Received update request, displaying update dialog");
            Log.d("blah", "" + UpdateContext.getUpdateContextFromIntentAction(intent.getAction()) );
            displayUpdateDialog(UpdateContext.getUpdateContextFromIntentAction(intent.getAction()));
        }
    };

    private void displayUpdateDialog(final UpdateContext updateContext){
        boolean isSelf = updateContext == UpdateContext.SELF;

        new OHDialogBuilder(this)
                .buildDialog(new OHDialogBuilder.OHDialogHandlerAdapter() {
                    @Override public void handlePositiveButtonResponse(Context context) {
                        checkPermissionAndDownload(updateContext);
                    }
                }, true)
                .withTitle(getString(isSelf ? R.string.oh_dialog_update_available_title : R.string.oh_dialog_recovery_update_available_title))
                .withContent(getString(isSelf? R.string.oh_dialog_update_available_text : R.string.oh_dialog_recovery_update_available_text))
                .withPositiveButtonText(getString( R.string.oh_dialog_update_available_positive_button_text))
                .withNegativeButtonText(getString(R.string.oh_dialog_update_available_negative_button_text))
                .withCloseButtonVisible(false)
                .show();
    }

    private void downloadUpdate(final UpdateContext updateContext){
        TaskResponseHandler<Boolean> resultHandler = new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                if(!result){
                    new OHDialogBuilder(OrderHubActivity.this)
                            .buildDialog(new OHDialogBuilder.OHDialogHandlerAdapter(){}, false)
                            .withTitle(getString(R.string.oh_dialog_update_error_title))
                            .withContent(getString(R.string.oh_dialog_update_error_text))
                            .withNeutralButtonText(getString(R.string.oh_alert_dialog_default_neutral_button_text))
                            .withCloseButtonVisible(true)
                            .show();
                }
            }
        };

        taskFactory.makeDownloadUpdateTask(updateContext, this, resultHandler).execute();
    }

    private void checkPermissionAndDownload(UpdateContext updateContext) {
        if (permissionUtil.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.d(TAG, "Already have WRITE_EXTERNAL_STORAGE permission, proceed to download update");
            downloadUpdate(updateContext);
        }
        else {
            Log.d(TAG, "Does not have WRITE_EXTERNAL_STORAGE permission, requesting");

            int requestCode = updateContext.getPermissionRequestCode();

            //since you have to either accept or deny permanently storage permission on app launch, if we reach this block
            //it means you have to run recovery anyways.  Leaving this in case there are exceptions.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        final UpdateContext uc = UpdateContext.getUpdateContextFromPermissionRequestCode(requestCode);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "User granted WRITE_EXTERNAL_STORAGE permission");

            if(uc == null){
                return;
            }

            Log.d(TAG, "Proceed to download update");
            downloadUpdate(uc);
        } else {
            Log.d(TAG, "User declined granting WRITE_EXTERNAL_STORAGE permission");

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Show permission explanation dialog...

                new OHDialogBuilder(this)
                        .buildDialog(new OHDialogBuilder.OHDialogHandlerAdapter() {
                            @Override
                            public void handleNeutralButtonResponse(Context context) {
                                if(uc != null) {
                                    checkPermissionAndDownload(uc);
                                }
                                else {
                                    ActivityCompat.requestPermissions(OrderHubActivity.this,
                                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                }
                            }
                        }, false)
                        .withTitle(getString(R.string.oh_dialog_update_warning_on_permission_denied))
                        .withContent(getString(R.string.oh_dialog_update_warning_text))
                        .withNeutralButtonText(getString(R.string.oh_dialog_update_warning_ok))
                        .withCloseButtonVisible(false)
                        .show();

            }else{
                //Never ask again selected, or device policy prohibits the app from having that permission.
                new OHDialogBuilder(this)
                        .buildDialog(null, false)
                        .withTitle(getString(R.string.oh_dialog_update_go_to_recovery_title))
                        .withContent(getString(R.string.oh_dialog_update_go_to_recovery_text))
                        .withNeutralButtonText(getString( R.string.oh_dialog_update_warning_ok))
                        .withCloseButtonVisible(false)
                        .show();
            }

        }
    }
}
