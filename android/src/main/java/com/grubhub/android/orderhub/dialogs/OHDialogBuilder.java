package com.grubhub.android.orderhub.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.widget.OrderHubDialog;

import org.apache.commons.lang3.StringUtils;

/**
 * A dialog builder that helps you create GrubHub themed dialog windows with custom title, text and button handlers.
 * NOTE: This is to be used only for simple dialogs that show static content. For complex ones, build your own.
 */
public class OHDialogBuilder {
    Context context;
    LayoutInflater layoutInflater;

    View dialogView;
    View twoButtonView;
    View singleButtonView;

    TextView positiveButton;
    TextView negativeButton;
    TextView neutralButton;
    View closeButton;

    TextView titleView;
    TextView contentView;

    OrderHubDialog dialog;

    public interface OHDialogHandler {
        void handleNeutralButtonResponse(Context context);
        void handlePositiveButtonResponse(Context context);
        void handleNegativeButtonResponse(Context context);
        void onClose(Context context);
    }

    // Convenience class in case you don't want to have to implement everything
    public static abstract class OHDialogHandlerAdapter implements OHDialogHandler {
        @Override
        public void handleNeutralButtonResponse(Context context) {

        }

        @Override
        public void handlePositiveButtonResponse(Context context) {

        }

        @Override
        public void handleNegativeButtonResponse(Context context) {

        }

        @Override
        public void onClose(Context context) {

        }
    }

    public OHDialogBuilder(final Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    /**
     * Builds a dialog and returns the OHDialogBuilder instance.
     * Use this if you have custom button text, need to get the dialog handle or want to control when you display the actual dialog
     * @return OHDialogBuilder
     */
    public OHDialogBuilder buildDialog(final OHDialogHandler ohDialogHandler, final boolean isTwoButton) {
        dialogView = layoutInflater.inflate(R.layout.oh_alert_dialog, null);

        titleView = (TextView) dialogView.findViewById(R.id.orderhub_alert_dialog_text_header);
        contentView = (TextView) dialogView.findViewById(R.id.orderhub_alert_dialog_text_sub_header);

        twoButtonView = dialogView.findViewById(R.id.alert_dialog_yes_no_layout);
        singleButtonView = dialogView.findViewById(R.id.alert_dialog_neutral_layout);

        positiveButton = (TextView) dialogView.findViewById(R.id.alert_dialog_ok_button);
        negativeButton = (TextView) dialogView.findViewById(R.id.alert_dialog_cancel_button);
        neutralButton = (TextView) dialogView.findViewById(R.id.alert_dialog_neutral_button);
        closeButton = dialogView.findViewById(R.id.alert_dialog_close_modal);

        dialog = new OrderHubDialog(context, R.style.Theme_Dialog_Translucent);
        dialog.setContentView(dialogView);

        dialog.setCancelable(false);

        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (ohDialogHandler != null) {
                    ohDialogHandler.onClose(context);
                }
            }
        });

        if (isTwoButton) {
            twoButtonView.setVisibility(View.VISIBLE);
            singleButtonView.setVisibility(View.GONE);
        }
        else {
            twoButtonView.setVisibility(View.GONE);
            singleButtonView.setVisibility(View.VISIBLE);
        }

        initDefaultStringValues();
        initSingleButtonHandler(ohDialogHandler, dialog);
        initTwoButtonHandler(ohDialogHandler, dialog);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        closeButton.setVisibility(View.GONE);

        return this;
    }

    /**
     * Builds and displays a simple dialog. Use this if you don't have custom content for the buttons.
     * @param title    Title for the dialog
     * @param content Text for the dialog
     * @param ohDialogHandler   DialogHandler for button response(s)
     * @param isTwoButton Is this a single button (OK) or a two button (Yes/No) dialog
     */
    public void buildAndDisplayDialog(final String title, final String content, final OHDialogHandler ohDialogHandler, final boolean isTwoButton) {
        buildDialog(ohDialogHandler, isTwoButton)
            .withTitle(title)
            .withContent(content)
            .show();
    }

    public OHDialogBuilder withCloseButtonVisible(boolean closeButtonVisible) {
        closeButton.setVisibility(closeButtonVisible ? View.VISIBLE : View.GONE);

        return this;
    }

    public OHDialogBuilder withTitle(String title) {
        titleView.setText(title);

        if (StringUtils.isEmpty(title)) {
            titleView.setVisibility(View.GONE);
        }
        else {
            titleView.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public OHDialogBuilder withContent(String text) {
        contentView.setText(text);

        if (StringUtils.isEmpty(text)) {
            contentView.setVisibility(View.GONE);
        }
        else {
            contentView.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public OHDialogBuilder withContent(SpannableStringBuilder sb) {
        contentView.setText(sb);


        if (StringUtils.isEmpty(sb.toString())) {
            contentView.setVisibility(View.GONE);
        }
        else {
            contentView.setVisibility(View.VISIBLE);
        }

        return this;
    }

    public OHDialogBuilder withPositiveButtonText(String positiveButtonText) {
        positiveButton.setText(positiveButtonText);
        return this;
    }

    public OHDialogBuilder withNegativeButtonText(String negativeButtonText) {
        negativeButton.setText(negativeButtonText);
        return this;
    }

    public OHDialogBuilder withNeutralButtonText(String neutralButtonText) {
        neutralButton.setText(neutralButtonText);
        return this;
    }

    public OHDialogBuilder withPositiveButtonTextSize(float size){
        positiveButton.setTextSize(size);
        return this;
    }

    public OHDialogBuilder withNeutralButtonTextSize(float size){
        neutralButton.setTextSize(size);
        return this;
    }

    public OHDialogBuilder withNegativeButtonTextSize(float size){
        negativeButton.setTextSize(size);
        return this;
    }

    public void show() {
        dialog.show();
    }

    public Dialog getDialog() {
        return dialog;
    }

    private void initSingleButtonHandler(final OHDialogHandler ohDialogHandler, final Dialog dialog) {
        singleButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ohDialogHandler != null) {
                    ohDialogHandler.handleNeutralButtonResponse(context);
                }
                dialog.dismiss();
            }
        });
    }

    private void initTwoButtonHandler(final OHDialogHandler ohDialogHandler, final Dialog dialog) {
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ohDialogHandler != null) {
                    ohDialogHandler.handlePositiveButtonResponse(context);
                }
                dialog.dismiss();
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ohDialogHandler != null) {
                    ohDialogHandler.handleNegativeButtonResponse(context);
                }
                dialog.dismiss();
            }
        });
    }

    private void initDefaultStringValues() {
        positiveButton.setText(context.getString(R.string.oh_alert_dialog_default_positive_button_text));
        negativeButton.setText(context.getString(R.string.oh_alert_dialog_default_negative_button_text));
        neutralButton.setText(context.getString(R.string.oh_alert_dialog_default_neutral_button_text));
    }
}
