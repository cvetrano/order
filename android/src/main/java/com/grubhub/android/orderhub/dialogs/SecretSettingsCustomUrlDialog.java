package com.grubhub.android.orderhub.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.widget.OrderHubDialog;

public class SecretSettingsCustomUrlDialog {

    public interface SecretSettingsCustomUrlDialogHandler {
        void dialogFinished(String gcUrl, String rftHost, String updateBucketUrl);
    }

    public class InvalidCustomEnvironmentException extends Exception{
        public InvalidCustomEnvironmentException(String msg){
            super(msg);
        }
    }

    private final Context context;
    private final LayoutInflater layoutInflater;

    @Inject
    public SecretSettingsCustomUrlDialog(final Context context, final LayoutInflater layoutInflater) {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    public void displaySecretSettingsUrlDialog(final OrderHubEnvironment env, final SecretSettingsCustomUrlDialogHandler handler, final Runnable cancelHandler) throws InvalidCustomEnvironmentException {
        if(!env.isCustom()){
            throw new InvalidCustomEnvironmentException("Only custom environments can be edited");
        }
        final OrderHubDialog dialog = new OrderHubDialog(context, R.style.Theme_Dialog_Translucent);
        final View contentView = layoutInflater.inflate(R.layout.secret_settings_customurl_dialog, null);

        final TextView gcUrlInput = (TextView)contentView.findViewById(R.id.secet_settings_customurl_dialog_enter_gc_url);
        final TextView rftHostInput = (TextView)contentView.findViewById(R.id.secet_settings_customurl_dialog_enter_rft_host);
        final TextView updateBucketUrlInput = (TextView)contentView.findViewById(R.id.secet_settings_customurl_dialog_enter_update_bucket_url);

        gcUrlInput.setText(env.getGrubcentralUrl());
        rftHostInput.setText(env.getRftHost());
        updateBucketUrlInput.setText(env.getUpdateBucketUrl());


        View closeModalButton = contentView.findViewById(R.id.secet_settings_customurl_dialog_close_btn);
        closeModalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                dialog.cancel();
            }
        });

        View submitButton = contentView.findViewById(R.id.secet_settings_customurl_dialog_submit_btn);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                handler.dialogFinished(gcUrlInput.getText().toString(), rftHostInput.getText().toString(), updateBucketUrlInput.getText().toString());
            }
        });

        dialog.setCancelable(true);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                cancelHandler.run();
            }
        });
        dialog.setContentView(contentView);
        dialog.show();
    }

}
