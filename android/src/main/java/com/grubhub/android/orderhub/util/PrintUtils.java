package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.inject.Singleton;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.Printer;
import com.grubhub.android.printing.star.PrinterFunctions;
import com.starmicronics.stario.PortInfo;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;

import java.util.ArrayList;
import java.util.List;

import roboguice.util.Ln;

//easily mockable synchronous print commands. these static methods are hard to mock so lets isolate them
@Singleton
public class PrintUtils {
    public List<Printer> getAvailablePrinters(){
        try {
            List<PortInfo> portList = StarIOPort.searchPrinter("TCP:");
            List<Printer> printers = new ArrayList<>();

            for (PortInfo port : portList) {
                printers.add(new Printer(port.getPortName(), port.getMacAddress(), port.getModelName()));
            }

            return printers;

        } catch (StarIOPortException se) {
            Ln.e(String.format("An error occurred while trying to find printers on the local network.  Error: %1$s", se.getMessage()));
        } catch (Exception e) {
            Ln.e(String.format("An error occurred.  Error: %1$s", e.getMessage()));
        }
        return null;
    }

    public PrintResult printImage(Context context, Printer printer, Bitmap printDataBitmap, int numCopies){
        return PrinterFunctions.PrintBitmap(context, printer.getIpAddress(), "", printDataBitmap, 650, true, numCopies);
    }
}
