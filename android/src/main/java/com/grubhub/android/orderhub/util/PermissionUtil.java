package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.os.Build.VERSION;

public class PermissionUtil {
    public boolean shouldAskPermission() {
        return VERSION.SDK_INT >= 23;
    }

    public boolean hasPermission(Context context, String requestedPermission) {
        return !shouldAskPermission() ||
                ContextCompat.checkSelfPermission(context, requestedPermission) == PackageManager.PERMISSION_GRANTED;
    }
}

