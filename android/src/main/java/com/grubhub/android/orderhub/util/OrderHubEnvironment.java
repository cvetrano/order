package com.grubhub.android.orderhub.util;

public enum OrderHubEnvironment {
    production("https://restaurant.grubhub.com", "https://rft.grubhub.com", "https://s3.amazonaws.com/gcinabox_update/", false,
            OrderHubEnvironment.PROD_GOOGLE_API_KEY),
    productionDebug("https://restaurant.grubhub.com", "https://rft.grubhub.com", "https://s3.amazonaws.com/gcinabox_update/", false,
            OrderHubEnvironment.PROD_GOOGLE_API_KEY),
    preprod("https://restaurant-pp.grubhub.com", "https://cert1-rft.ghbeta.com", false,
            OrderHubEnvironment.TEST_GOOGLE_API_KEY),
    customUrl("http://host.not.set", "http://host.not.set", true,
            OrderHubEnvironment.TEST_GOOGLE_API_KEY);

    private static final String PROD_GOOGLE_API_KEY = "UA-309051-45";
    private static final String TEST_GOOGLE_API_KEY = "UA-309051-69";

    private String grubcentralUrl;
    private String updateBucketUrl;
    private String rftHost;
    private final boolean custom;
    private String googleApiKey;

    static public String getRftBasePath(){
        return "/services/v2/";
    }
    static private String WIFI_PAGE = "/rft/wifi_success.jsp";;

    OrderHubEnvironment(String grubcentralUrl, String rftHost, boolean custom, String googleApiKey) {
        this(grubcentralUrl, rftHost, "https://s3.amazonaws.com/gcinabox_update-preprod/", custom, googleApiKey);
    }

    OrderHubEnvironment(String grubcentralUrl, String rftHost, String updateBucketUrl, boolean custom, String googleApiKey) {
        this.grubcentralUrl = grubcentralUrl;
        this.rftHost = rftHost;
        this.updateBucketUrl = updateBucketUrl;
        this.custom = custom;
        this.googleApiKey = googleApiKey;
    }

    public String getGrubcentralUrl() {
        return grubcentralUrl;
    }

    public String getRftUrl(){
        return rftHost + getRftBasePath();
    }

    public String getRftHost(){
        return rftHost;
    }

    public String getMigrationUrl(){
        return grubcentralUrl + "/orderhub-migration";
    }

    public String getUpdateBucketUrl(){
        return updateBucketUrl;
    }

    public boolean isCustom() {
        return custom;
    }

    public String getGoogleApiKey() {
        return googleApiKey;
    }

    public String getWifiAuthUrl(){
        return getRftHost().replace("https", "http") + WIFI_PAGE;
    }

    public void updateCustomUrls(String grubcentralUrl, String rftHost, String s3UpdateBucket){
        this.grubcentralUrl = grubcentralUrl;
        this.rftHost = rftHost;
        this.updateBucketUrl = s3UpdateBucket;
    }
}
