package com.grubhub.android.orderhub.analytics;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.util.DeviceUtils;

import javax.inject.Singleton;

/**
 * Analytics helper class
 */
@Singleton
public class Analytics {
    private final DeviceUtils deviceUtils;

    @Inject
    public Analytics(DeviceUtils deviceUtils) {
        this.deviceUtils = deviceUtils;
    }

    /**
     * Called when the application first starts up
     *
     * @param context Context to get application environment
     */
    public void onApplicationCreate(Context context) {
        String macAddress = deviceUtils.getMacAddress(context);
        Crashlytics.setString("mac_address", macAddress);
    }
}
