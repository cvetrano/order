package com.grubhub.android.orderhub.js;

import android.app.Activity;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.grubhub.android.orderhub.util.RecoveryUtils;
import com.grubhub.android.orderhub.util.UpdateContext;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import roboguice.util.Ln;

public class UpdateJs extends JsInterfaceBase {
    private RecoveryUtils recoveryUtils;

    public UpdateJs(Activity activity, WebView webView){
        super(activity, webView);
        recoveryUtils = getInjectable(RecoveryUtils.class);
    }

    @JavascriptInterface
    public String forceUpdateRecovery(){
        final String asyncId = generateAsyncId();
        UpdateContext updateContext = recoveryUtils.getRecoveryUpdateContext(activity);

        if(updateContext != null){
            taskFactory.makeDownloadUpdateTask(updateContext, activity, new TaskResponseHandler<Boolean>() {
                @Override
                public void handleResponse(Boolean result) {
                    if(result){
                        localSettings.setBoolean(PreferenceType.RecoveryUpdateTriedLastSession, true);
                    }
                    //just make sure the download succeeded. the installation will close the app anyway
                    sendAsyncResponse(asyncId, getBaseResponse(result));
                }
            }).execute();
        }
        else{
            JSONObject response = getBaseResponse(false);
            try{
                response.put("error", "recovery update not needed");
            }
            catch(JSONException e){
                Ln.e(e, "error returning response for no recovery update needed");
            }
            sendAsyncResponse(asyncId, response);
        }

        return asyncId;
    }

    @JavascriptInterface
    public boolean recoveryUpdateNeeded(){
        return recoveryUtils.getRecoveryUpdateContext(activity) != null;
    }
}
