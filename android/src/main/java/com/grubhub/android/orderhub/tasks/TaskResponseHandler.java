package com.grubhub.android.orderhub.tasks;

public interface TaskResponseHandler<T> {
    void handleResponse(T result);
}
