package com.grubhub.android.orderhub;

import android.app.Application;
import android.content.Intent;

import com.crashlytics.android.Crashlytics;
import com.google.inject.Provider;
import com.grubhub.android.orderhub.analytics.Analytics;
import com.grubhub.android.orderhub.services.UpdateService;

import io.fabric.sdk.android.Fabric;
import roboguice.RoboGuice;

public class OHApplication extends Application {

    static OHApplication instance;

    private Analytics analytics;

    public static OHApplication getInstance(){
        return instance;
    }

    public static class OHApplicationProvider implements Provider<OHApplication>{

        @Override public OHApplication get() {
            return getInstance();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        analytics = RoboGuice.getInjector(getApplicationContext()).getInstance(Analytics.class);
        Fabric.with(this, new Crashlytics());
        analytics.onApplicationCreate(getApplicationContext());
        instance = this;
        startService(new Intent(this, UpdateService.class));
    }

    @Override
    public void onTerminate() {
        stopService(new Intent(this, UpdateService.class));

        super.onTerminate();
    }
}
