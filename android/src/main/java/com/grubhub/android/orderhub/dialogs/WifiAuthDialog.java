package com.grubhub.android.orderhub.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.http.SslError;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.grubhub.android.orderhub.widget.OrderHubDialog;
import org.apache.commons.lang3.StringUtils;
import roboguice.util.Ln;

public class WifiAuthDialog {
    private final Context context;

    @Inject
    LayoutInflater layoutInflater;
    @Inject
    LocalSettings localSettings;

    View dialogContent;
    WebView webView;

    @Inject
    public WifiAuthDialog(final Context context, final LayoutInflater layoutInflater) {
        this.context = context;
        this.layoutInflater = layoutInflater;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void displayDialog(final Runnable onClose) {
        dialogContent = layoutInflater.inflate(R.layout.wifi_auth_dialog, null);

        final View closeButton = dialogContent.findViewById(R.id.wifi_auth_close_button);
        final View closeModalButton = dialogContent.findViewById(R.id.alert_dialog_close_modal);

        webView = (WebView)dialogContent.findViewById(R.id.wifi_auth_web_view);

        final OrderHubDialog dialog = new OrderHubDialog(context, R.style.Theme_Dialog_Translucent);
        dialog.setContentView(dialogContent);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onClose != null){
                    onClose.run();
                }
                dialog.dismiss();
            }
        });
        closeModalButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(onClose != null){
                    onClose.run();
                }
                dialog.dismiss();
            }
        });

        // Prevents a redirect from launching a browser and forces the webview to show the new page
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                return false;
            }
        });

        webView.clearCache(true);

        String url = localSettings.getCurrentEnvironment().getWifiAuthUrl();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        dialog.show();
    }
}