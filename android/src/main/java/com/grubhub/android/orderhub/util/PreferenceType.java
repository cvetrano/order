package com.grubhub.android.orderhub.util;

/**
 * Keys for preferences stored on the android device.  These values cannot be changed once
 * they have been deployed unless a migration ability is created to change any existing values.
 */
public enum PreferenceType {
    HasMigrated,

    Environment,
    CustomGrubcentralUrl,
    CustomRftHost,
    CustomUpdateBucketUrl,
    VersionName,
    DeviceId,
    ActiveRestaurantId,
    ActiveRestaurantName,

    LastUpdateCheck,
    RecoveryUpdateTriedLastSession,

    DefaultPrinterMacAddress,
    AlertSound
}
