package com.grubhub.android.orderhub.widget;

import com.grubhub.android.orderhub.R;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * An extension of GrubHubEditText (and EditText) that adds a small "X" button to the right of the
 * edit box to clear the text when pressed.
 */
public class ClearableEditText extends GrubhubEditText {

    final Drawable imgX = getResources().getDrawable(R.drawable.edit_clear); // X image

    public ClearableEditText(Context context) {
        super(context);
        init();
    }

    public ClearableEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public ClearableEditText(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        init();
    }

    void init()  {
        // Set bounds of our X button
        imgX.setBounds(0, 0, imgX.getIntrinsicWidth(), imgX.getIntrinsicHeight());

        // There may be initial text in the field, so we may need to display the button
        manageClearButton();

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ClearableEditText et = ClearableEditText.this;

                // Is there an X showing?
                if (et.getCompoundDrawables()[2] == null) {
                    return false;
                }

                // Only do this for up touches
                if (event.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }

                // Is touch on our clear button?
                if (event.getX() > et.getWidth() - et.getPaddingRight() - imgX.getIntrinsicWidth()) {
                    et.setText("");
                    ClearableEditText.this.manageClearButton();
                }
                return false;
            }
        });

        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ClearableEditText.this.manageClearButton();
            }

            @Override
            public void afterTextChanged(Editable arg0) { }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        });
    }

    void manageClearButton() {
        Drawable[] drawables = this.getCompoundDrawables();
        this.setCompoundDrawables(drawables[0],
                                  drawables[1],
                                  isBlank(this.getText().toString()) ? null : imgX,
                                  drawables[3]);
    }
}
