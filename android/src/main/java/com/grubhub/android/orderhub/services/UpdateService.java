package com.grubhub.android.orderhub.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.tasks.UpdateCheckTask;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.grubhub.android.orderhub.util.RecoveryUtils;
import com.grubhub.android.orderhub.util.UpdateContext;

import org.joda.time.DateTime;

import java.util.Timer;
import java.util.TimerTask;

import roboguice.service.RoboService;
import roboguice.util.Ln;

public class UpdateService extends RoboService {
    public static final String FULL_UPDATE_CHECK_INTENT_ACTION = "FULL_UPDATE_CHECK";
    @Inject
    LocalSettings localSettings;
    @Inject
    TaskFactory taskFactory;
    @Inject
    RecoveryUtils recoveryUtils;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    Timer updateTimer;
    TimerTask timerTask;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        Ln.i("UpdateService starting");
        setUpdateTask(intent == null); //will be null only if the os stops the service due to low memory and then restarts it
        LocalBroadcastManager.getInstance(this).registerReceiver(fullUpdateCheckListener, new IntentFilter(FULL_UPDATE_CHECK_INTENT_ACTION));
        return START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Ln.i("UpdateService destroying");
        updateTimer.cancel();
        timerTask.cancel();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fullUpdateCheckListener);
    }

    BroadcastReceiver fullUpdateCheckListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doFullUpdateCheck();
        }
    };

    private void doFullUpdateCheck(){
        taskFactory.makeUpdateCheckTask(getApplicationContext(), new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                if(result){
                    broadcastUpdateIntentForContext(UpdateContext.SELF);
                }
                else{
                    doRecoveryCheck();
                }
            }
        }).execute();
    }


    private void setUpdateTask(boolean baseFirstUpdateOnLastUpdateTime){
        updateTimer = new Timer("updateCheckThread", true);
        final long fixedDelay = 28800000;  //8 hours in ms
        long initialDelay = fixedDelay;

        if(baseFirstUpdateOnLastUpdateTime) {
            long timeSinceLastUpdate = new DateTime().toDate().getTime() - localSettings.getLong(PreferenceType.LastUpdateCheck, 0);
            initialDelay = Math.max(fixedDelay - timeSinceLastUpdate, 0); //negative delay should check immediately
        }
        timerTask = new TimerTask() {
            @Override
            public void run() {
                UpdateCheckTask updateCheckTask = taskFactory.makeUpdateCheckTask(getApplicationContext(), new TaskResponseHandler<Boolean>() {
                    @Override
                    public void handleResponse(Boolean result) {
                        if(result){
                            broadcastUpdateIntentForContext(UpdateContext.SELF);
                        }
                    }
                });
                updateCheckTask.execute();
            }
        };
        updateTimer.scheduleAtFixedRate(timerTask, initialDelay, fixedDelay);
    }

    private void doRecoveryCheck() {
        try {
            long installTime = getPackageManager().getPackageInfo(getPackageName(), 0).lastUpdateTime;
            if (System.currentTimeMillis() - installTime < 6 * 24 * 3600000) {  //don't update recovery if we updated this app in the last 6 days.
                return;
            }
        } catch (PackageManager.NameNotFoundException e) { //should never happen
            Ln.e(e, "Own Package not found checking for installation time. wtf?");
        }

        UpdateContext updateContext = recoveryUtils.getRecoveryUpdateContext(this);
        if (updateContext != null) {
            broadcastUpdateIntentForContext(updateContext);
        }
    }

    private void broadcastUpdateIntentForContext(UpdateContext updateContext){
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(updateContext.getUpdateIntent());
    }
}
