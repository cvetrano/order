package com.grubhub.android.orderhub.tasks;

import android.content.Context;

import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.DeviceUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import roboguice.RoboGuice;
import roboguice.util.Ln;


//returns true result if you should migrate
public class MigrationCheckTask extends AbstractOrderHubTask<Void, Void, Boolean> {
    private final TaskResponseHandler<Boolean> handler;

    private DeviceUtils deviceUtils;

    public MigrationCheckTask(Context context, TaskResponseHandler<Boolean> handler){
        super(context);
        this.handler = handler;
        deviceUtils = RoboGuice.getInjector(context).getInstance(DeviceUtils.class);
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        if(localSettings.getBoolean(PreferenceType.HasMigrated, false)){
            return false;
        }
        else{
            BufferedReader in = null;
            try {
                String macAddress = deviceUtils.getMacAddress(context);
                URL url = new URL(localSettings.getCurrentEnvironment().getRftUrl() + "device/" + macAddress + "/should_migrate");
                URLConnection conn = url.openConnection();
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder responseBuilder = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    responseBuilder.append(line);
                }

                boolean shouldMigrate = responseBuilder.toString().equals("true");

                if(!shouldMigrate){
                    localSettings.setBoolean(PreferenceType.HasMigrated, true);
                }
                return shouldMigrate;
            }
            catch(Exception e){
                return false; //dont want to constantly ask them to migrate on error. just assume they migrated
            }
            finally{
                if(in != null){
                    try{
                        in.close();
                    }
                    catch(Exception e){
                        Ln.w("error closing buffered reader after migration check");
                    }
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Boolean hasMigrated) {
        handler.handleResponse(hasMigrated);
    }
}
