package com.grubhub.android.orderhub.util;

import android.content.Intent;

public enum UpdateContext {
    SELF("gcinabox.apk", "com.grubhub.android.orderhub", "SELF_UPDATE_AVAILABLE", 1),
    RECOVERY("recovery_app.apk", "com.grubhub.android.orderhub.recovery", "RECOVERY_UPDATE_AVAILABLE", 2),
    RECOVERY_OLD_PACKAGE("recovery_app_old_package.apk", "com.grubhub.android.recovery", "RECOVERY_UPDATE_OLD_PACKAGE_AVAILABLE", 3);

    private final String filename;
    private final String packageName;
    private final String intentActionName;

    private final int permissionRequestCode;

    UpdateContext(String filename, String packageName, String intentActionName, int permissionRequestCode){
        this.filename = filename;
        this.packageName = packageName;
        this.intentActionName = intentActionName;
        this.permissionRequestCode = permissionRequestCode;
    }

    public String getPackageName(){
        return packageName;
    }

    public String getFilename() {
        return filename;
    }

    public Intent getUpdateIntent() {
        return new Intent(intentActionName);
    }

    public String getIntentActionName() {
        return intentActionName;
    }

    public int getPermissionRequestCode(){
        return permissionRequestCode;
    }

    public static UpdateContext getUpdateContextFromIntentAction(String intentAction){
        for(UpdateContext context : UpdateContext.values()){
            if(context.intentActionName.equals(intentAction)){
                return context;
            }
        }
        return null;
    }

    public static UpdateContext getUpdateContextFromPermissionRequestCode(int permissionRequestCode){
        for(UpdateContext context : UpdateContext.values()){
            if(context.permissionRequestCode == permissionRequestCode){
                return context;
            }
        }
        return null;
    }
}
