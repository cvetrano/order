package com.grubhub.android.orderhub.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TableLayout;

import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.js.DeviceInfoJs;
import com.grubhub.android.orderhub.js.MainJs;
import com.grubhub.android.orderhub.js.PrintJs;
import com.grubhub.android.orderhub.js.UpdateJs;

import java.util.Scanner;

import roboguice.inject.InjectView;

public class JsTestActivity extends OrderHubActivity {
    @InjectView(R.id.testWebView)
    protected FrameLayout webViewPlaceholder;

    WebView webView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.js_test_view);
        initWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        // Create the webview
        webView = new WebView(this);
        webView.setLayoutParams(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccess(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);

        // Attach the WebView to its placeholder
        webViewPlaceholder.addView(webView);
        webView.addJavascriptInterface(this, "jsTest");
        webView.addJavascriptInterface(new MainJs(this, webView), "gcInABox");
        webView.addJavascriptInterface(new PrintJs(this, webView), "gcInABoxPrinting");
        webView.addJavascriptInterface(new DeviceInfoJs(this, webView), "gcInABoxDevice");
        webView.addJavascriptInterface(new UpdateJs(this, webView), "gcInABoxUpdate");

        webView.loadUrl("file:///android_asset/js_test.html");
    }

    @JavascriptInterface
    public void exitTestPage(){
        startActivity(new Intent(this, WelcomeActivity.class));
        finish();
    }

    @JavascriptInterface
    public void reloadPage(){
        webView.post(new Runnable() {
            @Override
            public void run() {
                webView.reload();
            }
        });
    }

    @JavascriptInterface
    public String getTestImageEncoded() throws Exception{
        return new Scanner(getAssets().open("encodedTestImage")).useDelimiter("\\A").next();
    }
}
