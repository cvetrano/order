package com.grubhub.android.orderhub.tasks;

import android.content.Context;

import com.google.inject.Singleton;
import com.grubhub.android.orderhub.tasks.printing.GetPrinterListTask;
import com.grubhub.android.orderhub.tasks.printing.PrintImageTask;
import com.grubhub.android.orderhub.util.UpdateContext;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.Printer;

import java.util.List;

//this mostly just exists to make it easier to mock tasks in tests

@Singleton
public class TaskFactory {
    public MigrationCheckTask makeMigrationCheckTask(Context context, TaskResponseHandler<Boolean> handler){
        return new MigrationCheckTask(context, handler);
    }

    public UpdateCheckTask makeUpdateCheckTask(Context context, TaskResponseHandler<Boolean> handler){
        return new UpdateCheckTask(context, handler);
    }

    public DownloadUpdateTask makeDownloadUpdateTask(UpdateContext updateContext, Context context, TaskResponseHandler<Boolean> resultHandler){
        return new DownloadUpdateTask(updateContext, context, resultHandler);
    }

    public GetPrinterListTask makePrinterListTask(Context context, TaskResponseHandler<List<Printer>> handler){
        return new GetPrinterListTask(context, handler);
    }

    public PrintImageTask makePrintImageTask(Context context, Printer printer, String encodedImage, int numCopies, TaskResponseHandler<PrintResult> handler){
        return new PrintImageTask(context, printer, encodedImage, numCopies, handler);
    }

    public CaptivePortalCheckTask makeCaptivePortalCheck(Context context, TaskResponseHandler<Boolean> handler){
        return new CaptivePortalCheckTask(context, handler);
    }
}
