package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.util.ViewUtils;

/**
 * User: jsendelbach
 * Date: 2/18/12
 * Time: 3:35 PM
 */
public class GrubHubTextView extends TextView {
    private OrderHubFont font;

    public GrubHubTextView(final Context context) {
        super(context);
        configureFont(context);
        ViewUtils.fixBackgroundRepeat(this);
    }

    public GrubHubTextView(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
        readAttributes(context, attributeSet);
        configureFont(context);
        ViewUtils.fixBackgroundRepeat(this);
    }

    public GrubHubTextView(final Context context, final AttributeSet attributeSet, final int defStyle) {
        super(context, attributeSet, defStyle);
        readAttributes(context, attributeSet);
        configureFont(context);
        ViewUtils.fixBackgroundRepeat(this);
    }

    final void configureFont(final Context context) {
        if (font == null) {
            font = OrderHubFont.defaultFont();
        }
        setTypeface(font.getTypeFace(context));
    }

    final void readAttributes(final Context context, final AttributeSet attributeSet) {
        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.GrubhubTextView);
        final int index = typedArray.getInt(R.styleable.GrubhubTextView_font, OrderHubFont.DEFAULT_FONT_INDEX);
        font = OrderHubFont.fromCode(index);
        typedArray.recycle();
    }
}
