package com.grubhub.android.orderhub.widget;

/**
 * ngirardi 6/14/12
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.RadioButton;
import com.grubhub.android.orderhub.R;


public class GrubhubRadioButton extends RadioButton {
    private OrderHubFont font;

    public GrubhubRadioButton(final Context context) {
        super(context);
        configureFont(context);
    }

    public GrubhubRadioButton(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    public GrubhubRadioButton(final Context context, final AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    final void configureFont(final Context context) {
        if (font == null) {
            font = OrderHubFont.defaultFont();
        }
        setTypeface(font.getTypeFace(context));
    }

    final void readAttributes(final Context context, final AttributeSet attributeSet) {
        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.GrubhubTextView);
        final int index = typedArray.getInt(R.styleable.GrubhubTextView_font, OrderHubFont.DEFAULT_FONT_INDEX);
        font = OrderHubFont.fromCode(index);
        typedArray.recycle();
    }
}
