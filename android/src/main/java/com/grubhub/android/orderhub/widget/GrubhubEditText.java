package com.grubhub.android.orderhub.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;
import com.grubhub.android.orderhub.R;

/**
 * jsendelbach 3/18/12
 */
public class GrubhubEditText extends EditText {
    private OrderHubFont font;

    public GrubhubEditText(final Context context) {
        super(context);
        configureFont(context);
    }

    public GrubhubEditText(final Context context, final AttributeSet attributeSet) {
        super(context, attributeSet);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    public GrubhubEditText(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        readAttributes(context, attributeSet);
        configureFont(context);
    }

    public final void configureFont(final Context context) {
        if (font == null) {
            font = OrderHubFont.defaultFont();
        }
        setTypeface(font.getTypeFace(context));
    }

    final void readAttributes(final Context context, final AttributeSet attributeSet) {
        final TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.GrubhubTextView);
        final int index = typedArray.getInt(R.styleable.GrubhubTextView_font, OrderHubFont.DEFAULT_FONT_INDEX);
        font = OrderHubFont.fromCode(index);
        typedArray.recycle();
    }
}
