package com.grubhub.android.orderhub.widget;

import com.grubhub.android.orderhub.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class OrderHubDialogTest {
    OrderHubDialog orderHubDialog;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        orderHubDialog = new OrderHubDialog(RuntimeEnvironment.application.getApplicationContext());
    }

    @Test
    public void shouldConstructFromContext() throws Exception {
        // constructed in setup

        assertThat(orderHubDialog, is(not(nullValue())));
    }
}
