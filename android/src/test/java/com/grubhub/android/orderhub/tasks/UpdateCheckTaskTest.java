package com.grubhub.android.orderhub.tasks;

import android.content.Context;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class UpdateCheckTaskTest {
    UpdateCheckTask task;

    @Inject
    LocalSettings localSettings;

    Boolean taskResult;

    OrderHubEnvironment env;
    MockWebServer mockWebServer;

    DateTime now;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        now = new DateTime(2015, 10, 31, 0, 0, 0, 0);
        DateTimeUtils.setCurrentMillisFixed(now.getMillis());

        mockWebServer = new MockWebServer();
        mockWebServer.start();

        env = OrderHubEnvironment.customUrl;
        String baseHost = mockWebServer.url("/").toString();
        env.updateCustomUrls("gcurl", "rfthost", baseHost); //set the update bucket to the mock server
        when(localSettings.getCurrentEnvironment()).thenReturn(env);

        taskResult = null;
        task = new UpdateCheckTask(RuntimeEnvironment.application.getApplicationContext(), new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                taskResult = result;
            }
        });
    }

    @After
    public void shutDown() throws Exception {
        mockWebServer.shutdown();
    }

    private void enqueVersionCodeResponse(Integer versionCode){
        mockWebServer.enqueue(new MockResponse().setBody(versionCode.toString()).setResponseCode(200));
    }

    @Test
    public void setsLastUpdateTimeToNow() throws Exception {
        enqueVersionCodeResponse(123);
        task.execute();
        verify(localSettings).setLong(PreferenceType.LastUpdateCheck, now.toDate().getTime());
    }

    @Test
    public void makesRequestToProperFile() throws Exception {
        enqueVersionCodeResponse(123);
        task.execute();
        RecordedRequest request = mockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());
        assertEquals("/gcinabox.versioncode", request.getPath());
        assertEquals(1, mockWebServer.getRequestCount());
    }

    private Integer getCurrentVersionCode() throws Exception {
        Context context = RuntimeEnvironment.application.getApplicationContext();
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
    }

    @Test
    public void returnsTrueIfNewVersionCodeGreaterThanCurrentVersionCode() throws Exception {
        int currentVersionCode = getCurrentVersionCode();
        enqueVersionCodeResponse(currentVersionCode + 1);
        task.execute();
        assertTrue(taskResult);
    }

    @Test
    public void returnsFalseIfNewVersionCodeEqualToCurrentVersionCode() throws Exception {
        int currentVersionCode = getCurrentVersionCode();
        enqueVersionCodeResponse(currentVersionCode);
        task.execute();
        assertFalse(taskResult);
    }

    @Test
    public void returnsFalseIfNewVersionCodeLessThanCurrentVersionCode() throws Exception {
        int currentVersionCode = getCurrentVersionCode();
        enqueVersionCodeResponse(currentVersionCode - 1);
        task.execute();
        assertFalse(taskResult);
    }

    @Test
    public void returnsFalseIfNewVersionNotANumber() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody("This is not a number").setResponseCode(200));
        task.execute();
        assertFalse(taskResult);
    }

    @Test
    public void returnsFalseIfRequestFails() throws Exception {
        Integer newVersionCode = getCurrentVersionCode() + 1;
        mockWebServer.enqueue(new MockResponse().setBody(newVersionCode.toString()).setResponseCode(500));
        task.execute();
        assertFalse(taskResult);
    }
}