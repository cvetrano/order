package com.grubhub.android.orderhub.activities;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.test.InstrumentationTestCase;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.TestActivity;
import com.grubhub.android.orderhub.tasks.DownloadUpdateTask;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PermissionUtil;
import com.grubhub.android.orderhub.util.UpdateContext;

import org.junit.Before;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowDialog;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class OrderHubActivityTest {
    TestActivity activity;
    ShadowActivity shadowActivity;
    ShadowApplication shadowApplication;

    @Inject
    TaskFactory taskFactory;
    @Inject
    PermissionUtil permissionUtil;

    @Mock
    DownloadUpdateTask downloadUpdateTask;
    @Captor
    ArgumentCaptor<TaskResponseHandler<Boolean>> handlerCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(taskFactory.makeDownloadUpdateTask(any(UpdateContext.class), any(Context.class), Matchers.<TaskResponseHandler<Boolean>>any())).thenReturn(downloadUpdateTask);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        shadowActivity = Shadows.shadowOf(activity);
        shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);
    }

    private void clickUpdateDialogButton(Dialog dialog, boolean positive){
        dialog.findViewById(positive ? R.id.alert_dialog_ok_button : R.id.alert_dialog_cancel_button).performClick();
    }

    private void displayUpdateDialogForUpdateContext(UpdateContext updateContext){
        LocalBroadcastManager.getInstance(RuntimeEnvironment.application).sendBroadcast(updateContext.getUpdateIntent());
    }

    @Test
    public void broadcastingUpdateIntentOpensUpdateDialog() throws Exception{
        assertNull(ShadowDialog.getLatestDialog());
        displayUpdateDialogForUpdateContext(UpdateContext.SELF);
        assertNotNull(ShadowDialog.getLatestDialog());
    }

    @Test
    public void clickingNoSkipsUpdateTask() throws Exception {
        displayUpdateDialogForUpdateContext(UpdateContext.SELF);
        clickUpdateDialogButton(ShadowDialog.getLatestDialog(), false);
        verify(downloadUpdateTask, never()).execute();
    }

    @Test
    public void withWriteStoragePermissionGrantedclickingYesTriggersUpdateTaskWithCorrectUpdateContext() throws Exception {
        when(permissionUtil.hasPermission(any(OrderHubActivity.class), anyString())).thenReturn(true);
        displayUpdateDialogForUpdateContext(UpdateContext.SELF);
        clickUpdateDialogButton(ShadowDialog.getLatestDialog(), true);
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.SELF), eq(activity), handlerCaptor.capture());
        verify(downloadUpdateTask).execute();

        displayUpdateDialogForUpdateContext(UpdateContext.RECOVERY_OLD_PACKAGE);
        clickUpdateDialogButton(ShadowDialog.getLatestDialog(), true);
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.RECOVERY_OLD_PACKAGE), eq(activity), handlerCaptor.capture());
    }

    @Test
    public void updateTaskSuccessMeansNoMoreDialogs() throws Exception {
        when(permissionUtil.hasPermission(any(OrderHubActivity.class), anyString())).thenReturn(true);
        displayUpdateDialogForUpdateContext(UpdateContext.SELF);
        Dialog updateDialog = ShadowDialog.getLatestDialog();
        clickUpdateDialogButton(updateDialog, true);
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.SELF), eq(activity), handlerCaptor.capture());
        handlerCaptor.getValue().handleResponse(true);
        assertFalse(updateDialog.isShowing());
        assertEquals(updateDialog, ShadowDialog.getLatestDialog());
    }

    @Test
    public void updateTaskFailureDisplaysNewDialog() throws Exception {
        when(permissionUtil.hasPermission(any(OrderHubActivity.class), anyString())).thenReturn(true);
        displayUpdateDialogForUpdateContext(UpdateContext.SELF);
        Dialog updateDialog = ShadowDialog.getLatestDialog();
        clickUpdateDialogButton(updateDialog, true);
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.SELF), eq(activity), handlerCaptor.capture());
        handlerCaptor.getValue().handleResponse(false);
        Dialog downloadFailedDialog = ShadowDialog.getLatestDialog();
        assertNotEquals(updateDialog, downloadFailedDialog);
        assertTrue(downloadFailedDialog.isShowing());
    }
}