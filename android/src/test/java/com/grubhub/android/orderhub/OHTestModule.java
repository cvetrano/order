package com.grubhub.android.orderhub;

import com.google.inject.AbstractModule;
import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.util.RecoveryUtils;
import com.grubhub.android.orderhub.util.*;

import org.mockito.Mock;
import org.robolectric.RuntimeEnvironment;

import roboguice.inject.SharedPreferencesName;

import static org.mockito.Mockito.mock;


public class OHTestModule extends AbstractModule {

    protected void configure() {
        bind(ViewUtils.class).toInstance(mock(ViewUtils.class));
        bind(DeviceUtils.class).toInstance(mock(DeviceUtils.class));
        bind(RecoveryUtils.class).toInstance(mock(RecoveryUtils.class));
        bind(PrintUtils.class).toInstance(mock(PrintUtils.class));

        bind(LocalSettings.class).toInstance(mock(LocalSettings.class));

        bindConstant().annotatedWith(SharedPreferencesName.class).to("com.grubhub.android.orderhub.OrderHub");
        bind(TaskFactory.class).toInstance(mock(TaskFactory.class));
        bind(PermissionUtil.class).toInstance(mock(PermissionUtil.class));

        bind(OHApplication.class).toInstance((OHApplication) RuntimeEnvironment.application);
    }
}
