package com.grubhub.android.orderhub;

import com.google.inject.Injector;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.TestLifecycleApplication;
import roboguice.RoboGuice;

import java.lang.reflect.Method;

public class TestOHApplication extends OHApplication implements TestLifecycleApplication {

    @Override
    public void beforeTest(Method method) {
    }

    @Override
    public void prepareTest(Object test) {
        Injector injector = RoboGuice.overrideApplicationInjector(RuntimeEnvironment.application, new OHTestModule());
        injector.injectMembers(test);
    }

    @Override
    public void afterTest(Method method) {
    }

    @Override
    public void onCreate()
    {
        //deliberately don't call OHApp oncreate to avoid binding services, which wont work in test
    }

    @Override
    public void onTerminate()
    {
        //deliberately don't call OHApp onterminate to avoid unbinding services, which wont work in test
    }}
