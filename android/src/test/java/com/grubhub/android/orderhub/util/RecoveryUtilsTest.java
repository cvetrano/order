package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.grubhub.android.orderhub.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class RecoveryUtilsTest {
    RecoveryUtils recoveryUtils;

    @Mock
    Context context;

    @Mock
    PackageManager packageManager;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        recoveryUtils = new RecoveryUtils();
        when(context.getPackageManager()).thenReturn(packageManager);
    }

    private final int minimumNewRecoveryVersion = 600000000;
    private final String oldSettingsPackage = "com.grubhub.android.orderhub.settings.manager";

    @Test
    public void testGetRecoveryUpdateContext_oldPackage_OldVersion_returnsOldPackageContext() throws Exception {
        PackageInfo pi = new PackageInfo();
        pi.versionCode = minimumNewRecoveryVersion - 1;
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0)).thenReturn(pi);

        assertEquals(UpdateContext.RECOVERY_OLD_PACKAGE, recoveryUtils.getRecoveryUpdateContext(context));
    }

    @Test
    public void testGetRecoveryUpdateContext_oldPackage_NewVersion_returnsNull() throws Exception {
        PackageInfo pi = new PackageInfo();
        pi.versionCode = minimumNewRecoveryVersion;
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0)).thenReturn(pi);

        assertEquals(null, recoveryUtils.getRecoveryUpdateContext(context));
    }

    @Test
    public void testGetRecoveryUpdateContext_noOldPackageButOldSettings_returnsOldPackageContext() throws Exception {
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0)).thenThrow(new NameNotFoundException());
        when(packageManager.getPackageInfo(oldSettingsPackage, 0)).thenReturn(null); //it just can't throw

        assertEquals(UpdateContext.RECOVERY_OLD_PACKAGE, recoveryUtils.getRecoveryUpdateContext(context));
    }

    @Test
    public void testGetRecoveryUpdateContext_noRecovery_returnsPackageContext() throws Exception {
        when(packageManager.getPackageInfo(anyString(), eq(0))).thenThrow(new NameNotFoundException());

        assertEquals(UpdateContext.RECOVERY, recoveryUtils.getRecoveryUpdateContext(context));
    }

    @Test
    public void testGetRecoveryUpdateContext_newPackageRecovery_oldVersion_returnsPackageContext() throws Exception {
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0)).thenThrow(new NameNotFoundException());
        when(packageManager.getPackageInfo(oldSettingsPackage, 0)).thenThrow(new NameNotFoundException());

        PackageInfo pi = new PackageInfo();
        pi.versionCode = minimumNewRecoveryVersion - 1;
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY.getPackageName(), 0)).thenReturn(pi);

        assertEquals(UpdateContext.RECOVERY, recoveryUtils.getRecoveryUpdateContext(context));
    }

    @Test
    public void testGetRecoveryUpdateContext_newPackageRecovery_newVersion_returnsNull() throws Exception {
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY_OLD_PACKAGE.getPackageName(), 0)).thenThrow(new NameNotFoundException());
        when(packageManager.getPackageInfo(oldSettingsPackage, 0)).thenThrow(new NameNotFoundException());

        PackageInfo pi = new PackageInfo();
        pi.versionCode = minimumNewRecoveryVersion;
        when(packageManager.getPackageInfo(UpdateContext.RECOVERY.getPackageName(), 0)).thenReturn(pi);

        assertEquals(null, recoveryUtils.getRecoveryUpdateContext(context));
    }
}