package com.grubhub.android.orderhub.util;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LocalSettingsTest {

    LocalSettings localSettings;

    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    Context context;
    @Mock
    SecurityUtils securityUtils;


    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(securityUtils.createSalt(context)).thenReturn("FooBar".getBytes("UTF-8"));
        when(securityUtils.base64EncodeToString(any(byte[].class), anyInt())).thenReturn("FooBarEncoded");
        when(securityUtils.base64Decode(anyString(), anyInt())).thenReturn("FooBar".getBytes("UTF-8"));

        localSettings = new LocalSettings(context, sharedPreferences, securityUtils);
        localSettings.sharedPreferences = sharedPreferences;
    }

    @Test
    public void testString() throws Exception {
        String testValue = "abcdefg";
        assertThat(localSettings.getString(PreferenceType.CustomGrubcentralUrl), is(nullValue()));

        localSettings.set(PreferenceType.CustomGrubcentralUrl, testValue);
        assertThat(localSettings.getString(PreferenceType.CustomGrubcentralUrl), is(testValue));
    }

    @Test
    public void testNull() throws Exception {
        localSettings.set(PreferenceType.CustomGrubcentralUrl, null);
        assertThat(localSettings.getString(PreferenceType.CustomGrubcentralUrl), nullValue());
        localSettings.setLong(PreferenceType.CustomGrubcentralUrl, null);
        assertThat(localSettings.getLong(PreferenceType.CustomGrubcentralUrl), is(-1l));
    }

    @Test
    public void testEnvironment() throws Exception {
        // First call is null, which defaults to prod
        assertThat(localSettings.getCurrentEnvironment(), is(OrderHubEnvironment.production));

        localSettings.setEnvironment(OrderHubEnvironment.preprod);
        assertThat(localSettings.getCurrentEnvironment(), is(OrderHubEnvironment.preprod));
    }

    @Test
    public void ensureAesKeyNameIsIdempotent() throws InvalidKeySpecException,NoSuchAlgorithmException {
        String keyName1 = localSettings.generateAesKeyName(context);
        String keyName2 = localSettings.generateAesKeyName(context);

        assertThat(keyName1, equalTo(keyName2));
    }

    @Test
    public void ensureAesValueIsIdempotent() throws NoSuchAlgorithmException {
        String keyValue1 = localSettings.generateAesKeyValue();
        String keyValue2 = localSettings.generateAesKeyValue();

        assertThat(keyValue1, equalTo(keyValue2));
    }

    @Ignore("according to https://groups.google.com/forum/?fromgroups#!topic/robolectric/84id_6vXbUI may need to create shadow of Base64 since Base64.encodeToString always returns null")
    @Test
    public void ensureEncryptDecryptReturnsOriginal() {
        String clearText = "FooBar";
        String encryptedText = localSettings.encrypt(clearText);
        String decryptedText = localSettings.decrypt(encryptedText);

        assertFalse(clearText.equals(encryptedText));
        assertFalse(encryptedText.equals(decryptedText));
        assertTrue(clearText.equals(decryptedText));
    }


}
