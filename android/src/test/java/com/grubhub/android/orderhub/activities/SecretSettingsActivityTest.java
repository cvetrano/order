package com.grubhub.android.orderhub.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.dialogs.SecretSettingsCustomUrlDialog;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.LocalSettings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SecretSettingsActivityTest {
    @Inject LocalSettings localSettings;
    @Inject LayoutInflater layoutInflater;
    @Mock SecretSettingsCustomUrlDialog secretSettingsCustomUrlDialog;

    // subject of the test
    SecretSettingsActivity activity;
    ShadowActivity shadowActivity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(localSettings.getCurrentEnvironment()).thenReturn(OrderHubEnvironment.production);
        activity = Robolectric.buildActivity(SecretSettingsActivity.class).create().start().get();

        shadowActivity = Shadows.shadowOf(activity);
    }

    private void verifyUrlsForEnv(OrderHubEnvironment env){
        assertEquals(activity.gcUrl.getText(), env.getGrubcentralUrl());
        assertEquals(activity.rftHost.getText(), env.getRftHost());
        assertEquals(activity.updateBucketUrl.getText(), env.getUpdateBucketUrl());
    }

    @Test
    public void textItemsDisplayUrlsFromEnv(){
        verifyUrlsForEnv(OrderHubEnvironment.production);
    }

    @Test
    public void closeButtonGoesToWelcomeActivity(){
        activity.closeButton.performClick();
        Intent shadowIntent = shadowActivity.peekNextStartedActivityForResult().intent;
        assertEquals(new ComponentName(activity, WelcomeActivity.class), shadowIntent.getComponent());
    }

    @Test
    public void changeEnvironmentShowsEnvironmentListAlertDialog(){
        activity.chooseEnvFromList.performClick();
        ShadowAlertDialog sAlert = Shadows.shadowOf(ShadowAlertDialog.getLatestAlertDialog());
        assertEquals(sAlert.getTitle(), "Choose Environment");
        List<CharSequence> alertItems = Arrays.asList(sAlert.getItems());
        for(OrderHubEnvironment env : OrderHubEnvironment.values()){
            assertTrue(alertItems.contains(env.name()));
        }
    }

    private void clickEnvInList(int index) throws Exception{
        activity.chooseEnvFromList.performClick();
        ShadowAlertDialog sAlert = Shadows.shadowOf(ShadowAlertDialog.getLatestAlertDialog());
        //this relies on the list of orderhub environments being long enough
        //this is easier than making a mock list of environments
        if(OrderHubEnvironment.values().length <= index)
        {
            throw new Exception("Invalid orderhub envs");
        }
        sAlert.clickOnItem(index);
    }

    @Test
    public void clickingEnvironmentInListChangesUrlsAndEnv() throws Exception{
        clickEnvInList(1);
        OrderHubEnvironment env = OrderHubEnvironment.values()[1];
        verifyUrlsForEnv(env);
        verify(localSettings).setEnvironment(env);
    }

    private int getIndexOfCustomOrderhubEnv() throws Exception {
        int ind = 0;
        OrderHubEnvironment[] envs = OrderHubEnvironment.values();
        for(; ind < OrderHubEnvironment.values().length; ind++){
            if(envs[ind].isCustom()) {
                break;
            }
        }
        if(ind >= envs.length || !envs[ind].isCustom()){
            throw new Exception("invalid orderhub envs");
        }
        return ind;
    }

    private void openUrlSelectionDialog() throws Exception {
        //this relies on there being at least one custom environment. again, easier than making a mock list of envs
        int customIndex = getIndexOfCustomOrderhubEnv();
        clickEnvInList(customIndex);
    }

    @Test
    public void clickingCustomEnvOpensUrlSelectionDialogAndSetsEnv() throws Exception{
        activity.secretSettingsCustomUrlDialog = secretSettingsCustomUrlDialog;
        openUrlSelectionDialog();
        OrderHubEnvironment env = OrderHubEnvironment.values()[getIndexOfCustomOrderhubEnv()];
        verify(activity.secretSettingsCustomUrlDialog).displaySecretSettingsUrlDialog(eq(env),
                any(SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler.class),
                any(Runnable.class));
        verify(localSettings).setEnvironment(env);
    }

    private void cancelCustomUrlDialog() throws Exception{
        activity.secretSettingsCustomUrlDialog = secretSettingsCustomUrlDialog;
        ArgumentCaptor<Runnable> cancelCaptor = ArgumentCaptor.forClass(Runnable.class);
        openUrlSelectionDialog();
        verify(activity.secretSettingsCustomUrlDialog).displaySecretSettingsUrlDialog(any(OrderHubEnvironment.class),
                any(SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler.class),
                cancelCaptor.capture());
        cancelCaptor.getValue().run();
    }

    @Test
    public void cancellingCustomUrlDialogRevertsToPreviousEnv() throws Exception{
        cancelCustomUrlDialog();
        verify(localSettings).setEnvironment(OrderHubEnvironment.production);
        verifyUrlsForEnv(OrderHubEnvironment.production);
    }

    @Test
    public void cancellingCustomUrlDialogRevertsToPreviousEnv_evenAfterSelectingAnotherEnv() throws Exception{
        clickEnvInList(1);
        OrderHubEnvironment previousEnv = OrderHubEnvironment.values()[1];

        cancelCustomUrlDialog();

        verify(localSettings, times(2)).setEnvironment(previousEnv);
        verifyUrlsForEnv(previousEnv);
    }

    @Test
    public void finishingCustomUrlDialogUpdatesEnvAndDialog() throws Exception{
        activity.secretSettingsCustomUrlDialog = secretSettingsCustomUrlDialog;
        ArgumentCaptor<SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler> handlerCaptor =
                ArgumentCaptor.forClass(SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler.class);
        openUrlSelectionDialog();
        verify(activity.secretSettingsCustomUrlDialog).displaySecretSettingsUrlDialog(any(OrderHubEnvironment.class),
                handlerCaptor.capture(),
                any(Runnable.class));
        handlerCaptor.getValue().dialogFinished("gcurl", "rfthost", "updatebucketurl");
        OrderHubEnvironment env = OrderHubEnvironment.values()[getIndexOfCustomOrderhubEnv()];
        assertEquals("gcurl", env.getGrubcentralUrl());
        assertEquals("rfthost", env.getRftHost());
        assertEquals("updatebucketurl", env.getUpdateBucketUrl());
        verifyUrlsForEnv(env);
    }

    @Test
    public void clearPrefsClearsLocalSettings(){
        activity.clearPreferences.performClick();
        verify(localSettings).clearPreferences(null);
    }

    @Test
    public void clearMigrationFlagBtnClearsMigrationFlag(){
        activity.clearMigrationFlag.performClick();
        verify(localSettings).clearPreference(eq(PreferenceType.HasMigrated));
    }

    @Test
    public void setMigrationFlagBtnSetsMigrationFlagToTrue(){
        activity.setMigrationFlag.performClick();
        verify(localSettings).setBoolean(eq(PreferenceType.HasMigrated), eq(true));
    }

    @Test
    public void testCreateSecretSettingsDialogAlertBuilder(){
        SecretSettingsActivity.createSecretSettingsDialogBuilder(layoutInflater, activity, "header string").show();
        ShadowAlertDialog sAlert = Shadows.shadowOf(ShadowAlertDialog.getLatestAlertDialog());
        TextView headerText = (TextView)sAlert.getCustomTitleView().findViewById(R.id.header_text);
        assertEquals("header string", headerText.getText());
    }
}