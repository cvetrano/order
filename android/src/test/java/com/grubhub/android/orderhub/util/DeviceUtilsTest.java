package com.grubhub.android.orderhub.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.PowerManager;

import com.grubhub.android.orderhub.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowPowerManager;
import org.robolectric.shadows.ShadowWifiManager;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DeviceUtilsTest {
    DeviceUtils deviceUtils;

    ShadowWifiManager shadowWifiManager;

    ShadowPowerManager shadowPowerManager;

    @Mock
    Context context;

    @Mock
    Intent batteryIntent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        ShadowApplication shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);
        WifiManager wifiManager = (WifiManager) shadowApplication.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        shadowWifiManager = Shadows.shadowOf(wifiManager);
        Shadows.shadowOf(shadowWifiManager.getConnectionInfo()).setMacAddress("foobar");

        PowerManager powerManager = (PowerManager) shadowApplication.getApplicationContext().getSystemService(Context.POWER_SERVICE);
        shadowPowerManager = Shadows.shadowOf(powerManager);

        when(context.registerReceiver(null, DeviceUtils.BATTERY_CHANGED_INTENT_FILTER))
                .thenReturn(batteryIntent);

        deviceUtils = new DeviceUtils();
    }

    @Test
    public void returnsMacAddress() throws Exception {
        assertEquals("foobar", deviceUtils.getMacAddress(RuntimeEnvironment.application.getApplicationContext()));
    }

    @Test
    public void testGetBatteryLevel() throws Exception {
        when(batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)).thenReturn(50);
        assertEquals(50, deviceUtils.getBatteryLevel(context));
    }

    @Test
    public void testIsCharging() throws Exception {
        when(batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_CHARGING);
        assertTrue(deviceUtils.isCharging(context));

        when(batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)).thenReturn(BatteryManager.BATTERY_STATUS_NOT_CHARGING);
        assertFalse(deviceUtils.isCharging(context));
    }

    @Test
    public void testIsScreenOn() throws Exception {
        shadowPowerManager.setIsScreenOn(false);
        assertFalse(deviceUtils.isScreenOn(RuntimeEnvironment.application.getApplicationContext()));

        shadowPowerManager.setIsScreenOn(true);
        assertTrue(deviceUtils.isScreenOn(RuntimeEnvironment.application.getApplicationContext()));
    }

    @Test
    public void testIsAppOpen() throws Exception {
        DeviceUtils.setIsActive(true);
        assertTrue(deviceUtils.isAppOpen());

        DeviceUtils.setIsActive(false);
        assertFalse(deviceUtils.isAppOpen());
    }
}