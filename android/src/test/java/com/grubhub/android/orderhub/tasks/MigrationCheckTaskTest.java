package com.grubhub.android.orderhub.tasks;

import android.content.Context;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MigrationCheckTaskTest {
    MigrationCheckTask task;

    @Inject
    LocalSettings localSettings;

    @Inject
    DeviceUtils deviceUtils;

    Boolean taskResult;

    OrderHubEnvironment env;
    MockWebServer mockWebServer;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mockWebServer = new MockWebServer();
        mockWebServer.start();

        env = OrderHubEnvironment.customUrl;
        String baseHost = mockWebServer.url("/").toString();
        baseHost = baseHost.substring(0, baseHost.length()-1); //remove trailing slash
        env.updateCustomUrls("gcurl", baseHost, "updateBucketUrl");
        when(localSettings.getCurrentEnvironment()).thenReturn(env);

        taskResult = null;
        task = new MigrationCheckTask(RuntimeEnvironment.application.getApplicationContext(), new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                taskResult = result;
            }
        });
    }

    @After
    public void shutDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void returnsFalseWithNoHttpRequestIfFlagIsPresent() throws Exception {
        when(localSettings.getBoolean(PreferenceType.HasMigrated, false)).thenReturn(true);
        task.execute();
        assertFalse(taskResult);
        assertEquals(0, mockWebServer.getRequestCount());
    }

    @Test
    public void returnsValueFromRftRequestIfFlagIsNotPresent_falseTest() throws Exception {
        when(localSettings.getBoolean(PreferenceType.HasMigrated, false)).thenReturn(false);
        String macAddr = "mac_addr";
        when(deviceUtils.getMacAddress(any(Context.class))).thenReturn(macAddr);
        mockWebServer.enqueue(new MockResponse().setBody("false").setResponseCode(200));
        task.execute();
        RecordedRequest request = mockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());
        assertEquals(OrderHubEnvironment.getRftBasePath() + "device/" + macAddr + "/should_migrate", request.getPath());
        assertFalse(taskResult);
        assertEquals(1, mockWebServer.getRequestCount());
    }

    @Test
    public void returnsValueFromRftRequestIfFlagIsNotPresent_trueTest() throws Exception {
        when(localSettings.getBoolean(PreferenceType.HasMigrated, false)).thenReturn(false);
        String macAddr = "mac_addr";
        when(deviceUtils.getMacAddress(any(Context.class))).thenReturn(macAddr);
        mockWebServer.enqueue(new MockResponse().setBody("true").setResponseCode(200));
        task.execute();
        assertTrue(taskResult);
    }

    @Test
    public void returnsFalseAndDoesntCrashWhenError() throws Exception {
        when(localSettings.getBoolean(PreferenceType.HasMigrated, false)).thenReturn(false);
        String macAddr = "mac_addr";
        when(deviceUtils.getMacAddress(any(Context.class))).thenReturn((macAddr));
        mockWebServer.enqueue(new MockResponse().setBody("true").setResponseCode(500));
        task.execute();
        assertFalse(taskResult);
    }

}