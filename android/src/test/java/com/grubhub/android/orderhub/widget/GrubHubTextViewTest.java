package com.grubhub.android.orderhub.widget;

import android.util.AttributeSet;
import com.grubhub.android.orderhub.BuildConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * User: jsendelbach
 * Date: 2/18/12
 * Time: 3:44 PM
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GrubHubTextViewTest {

    GrubHubTextView grubHubTextView;

    AttributeSet attributeSet;
    int styleId;

    @Before
    public void setup() throws Exception {
        attributeSet = mock(AttributeSet.class);
    }

    @Test
    public void shouldConstructFromContext() throws Exception {
        grubHubTextView = new GrubHubTextView(RuntimeEnvironment.application.getApplicationContext());
        assertThat(grubHubTextView, is(not(nullValue())));
        // TODO - how to assert font?
    }

    @Test
    public void shouldConstructFromContextAndAttributes() throws Exception {
        grubHubTextView = new GrubHubTextView(RuntimeEnvironment.application.getApplicationContext(), attributeSet);
        assertThat(grubHubTextView, is(not(nullValue())));
    }

    @Test
    public void shouldConstructFromContextAndAttributesAndStyle() throws Exception {
        styleId = 0;
        grubHubTextView = new GrubHubTextView(RuntimeEnvironment.application.getApplicationContext(), attributeSet, styleId);
        assertThat(grubHubTextView, is(not(nullValue())));
    }


}
