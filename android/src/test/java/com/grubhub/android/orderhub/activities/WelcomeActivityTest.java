package com.grubhub.android.orderhub.activities;

import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.TableLayout;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.util.LocalSettings;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@Ignore("waiting for robolectric 3.1. see https://github.com/robolectric/robolectric/pull/2101")
public class WelcomeActivityTest {
    WelcomeActivity activity;
    ShadowActivity shadowActivity;

    @Inject
    LocalSettings localSettings;

    @Inject
    TaskFactory taskFactory;

    DeviceUtils deviceUtils = new DeviceUtils();
    OrderHubEnvironment env;
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        env = localSettings.getCurrentEnvironment();
        activity = Robolectric.buildActivity(WelcomeActivity.class).create().get();
        shadowActivity = Shadows.shadowOf(activity);
    }

    @Test
    public void loaderWebViewLoadsImmediately() throws Exception {
        assertEquals(new ViewGroup.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT), activity.loaderWebView.getLayoutParams());
        assertTrue(activity.loaderWebView.getSettings().getLoadsImagesAutomatically());
        assertEquals(WebSettings.LOAD_NO_CACHE, activity.loaderWebView.getSettings().getCacheMode());
        assertEquals("file:///android_asset/loader.html", activity.loaderWebView.getUrl());
        assertEquals(1, activity.loaderWebViewPlaceholder.getChildCount());
        assertEquals(activity.loaderWebView, activity.loaderWebViewPlaceholder.getChildAt(0));
    }

    @Test
    public void testIsActiveSetToTrueOnResume(){
        activity.onResume();
        assertTrue(deviceUtils.isAppOpen());
    }

    @Test
    public void testIsActiveSetToFalseOnPause(){
        activity.onPause();
        assertFalse(deviceUtils.isAppOpen());
    }
}