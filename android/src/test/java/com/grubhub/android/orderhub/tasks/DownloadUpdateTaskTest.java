package com.grubhub.android.orderhub.tasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;
import com.grubhub.android.orderhub.util.LocalSettings;
import com.grubhub.android.orderhub.util.UpdateContext;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.apache.maven.artifact.ant.shaded.IOUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowApplication;
import org.robolectric.shadows.ShadowProgressDialog;


import java.io.File;
import java.io.FileInputStream;
import okio.Buffer;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DownloadUpdateTaskTest {
    DownloadUpdateTask task;
    UpdateContext updateContext;

    @Inject
    LocalSettings localSettings;

    Boolean taskResult;

    OrderHubEnvironment env;
    MockWebServer mockWebServer;

    DateTime now;

    ShadowApplication shadowApplication;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        now = new DateTime(2015, 10, 31, 0, 0, 0, 0);
        DateTimeUtils.setCurrentMillisFixed(now.getMillis());

        shadowApplication = Shadows.shadowOf(RuntimeEnvironment.application);

        mockWebServer = new MockWebServer();
        mockWebServer.start();

        env = OrderHubEnvironment.customUrl;
        String baseHost = mockWebServer.url("/").toString();
        env.updateCustomUrls("gcurl", "rfthost", baseHost); //set the update bucket to the mock server
        when(localSettings.getCurrentEnvironment()).thenReturn(env);

        taskResult = null;
        setTaskForUpdateContext(UpdateContext.SELF);
    }

    private void setTaskForUpdateContext(UpdateContext context){
        updateContext = context;
        task = new DownloadUpdateTask(context, RuntimeEnvironment.application, new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                taskResult = result;
            }
        });
    }

    @After
    public void shutDown() throws Exception {
        mockWebServer.shutdown();
    }

    @Test
    public void opensProgressDialogBeforeDownloadStarts() throws Exception {
        task.onPreExecute();
        ShadowAlertDialog shadowAlertDialog = shadowApplication.getLatestAlertDialog();
        assertTrue(shadowAlertDialog instanceof ShadowProgressDialog);
        ShadowProgressDialog shadowProgressDialog = (ShadowProgressDialog)shadowAlertDialog;
        assertFalse(shadowProgressDialog.isCancelable());
        assertEquals(RuntimeEnvironment.application.getString(R.string.app_update_downloading), shadowProgressDialog.getMessage());
        //can't find a method on dialog to check that style is horizontal :(
        assertEquals(0, task.progressDialog.getProgress());
        assertEquals(100, task.progressDialog.getMax());
        assertTrue(task.progressDialog.isShowing());
    }

    @Test
    public void createsDownloadFolderIfNotExistingBeforeDownloadStarts() throws Exception {
        File f = new File(Environment.getExternalStorageDirectory() + "/Download/");
        f.delete();
        task.onPreExecute();
        assertTrue(f.exists() && f.isDirectory());
    }

    private Buffer getTestBinary(int len){
        byte[] buffer = new byte[len];
        for(int i = 0; i<len; i++){
            buffer[i] = (byte) i;
        }
        return new Buffer().write(buffer);
    }

    private void enqueResponse(Buffer fileContents){
        mockWebServer.enqueue(new MockResponse().setBody(fileContents).setResponseCode(200));
    }

    @Test
    public void makesRequestToProperFile() throws Exception {
        Buffer fileContents = getTestBinary(12345);
        enqueResponse(fileContents);
        task.execute();
        RecordedRequest request = mockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());
        assertEquals("/" + updateContext.getFilename(), request.getPath());
        assertEquals(1, mockWebServer.getRequestCount());

        setTaskForUpdateContext(UpdateContext.RECOVERY);
        enqueResponse(fileContents);
        task.execute();
        request = mockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());
        assertEquals("/" + updateContext.getFilename(), request.getPath());
        assertEquals(2, mockWebServer.getRequestCount());
    }

    @Test
    public void writesFileToDownloadDir() throws Exception {
        setTaskForUpdateContext(UpdateContext.RECOVERY_OLD_PACKAGE);
        File f = new File(Environment.getExternalStorageDirectory() + "/Download/", updateContext.getFilename());
        f.delete();
        Buffer fileContents = getTestBinary(12345);
        enqueResponse(fileContents);
        task.execute();
        assertArrayEquals(fileContents.readByteArray(), IOUtil.toByteArray(new FileInputStream(f)));
    }

    @Test
    public void updatesProgressEvery8192bytes() throws Exception {
        task.progressDialog = mock(DownloadUpdateTask.AttachedStatusProgressDialog.class);
        int fileSize = 16385; //2 full segments plus a byte: 3 total updates
        Buffer fileContents = getTestBinary(fileSize);
        enqueResponse(fileContents);
        task.execute();
        verify(task.progressDialog).setProgress(0); //from onpreexecute
        verify(task.progressDialog).setProgress(8192 * 100 / fileSize);
        verify(task.progressDialog).setProgress(16384 * 100 / fileSize);
        verify(task.progressDialog).setProgress(100);
        verify(task.progressDialog, times(4)).setProgress(anyInt());
    }

    @Test
    public void returnsTrueAfterSuccessfullDownload() throws Exception {
        enqueResponse(getTestBinary(12345));
        task.execute();
        assertTrue(taskResult);
    }

    @Test
    public void returnsFalseAfterFailedDownload() throws Exception {
        mockWebServer.enqueue(new MockResponse().setBody(getTestBinary(12345)).setResponseCode(500));
        task.execute();
        assertFalse(taskResult);
    }

    @Test
    public void returnsFalseIfEmptyResponse() throws Exception {
        enqueResponse(getTestBinary(0));
        task.execute();
        assertFalse(taskResult);
    }

    @Test
    public void closesProgressDialogAfterDone() throws Exception {
        enqueResponse(getTestBinary(12345));
        task.execute();
        assertFalse(task.progressDialog.isShowing());
    }

    @Test
    public void installsNewApkIfSuccessful() throws Exception {
        enqueResponse(getTestBinary(12345));
        task.execute();
        Intent intent = shadowApplication.getNextStartedActivity();
        File f = new File(Environment.getExternalStorageDirectory() + "/Download/", updateContext.getFilename());
        assertEquals(Intent.ACTION_VIEW, intent.getAction());
        assertEquals(Uri.fromFile(f), intent.getData());
        assertEquals("application/vnd.android.package-archive", intent.getType());
    }

    @Test
    public void setsNewTaskFlagForNonSelfUpdate() throws Exception {
        enqueResponse(getTestBinary(12345));
        task.execute();
        Intent intent = shadowApplication.getNextStartedActivity();
        assertEquals(Intent.FLAG_ACTIVITY_NEW_TASK & intent.getFlags(), 0);

        setTaskForUpdateContext(UpdateContext.RECOVERY);
        enqueResponse(getTestBinary(12345));
        task.execute();
        intent = shadowApplication.getNextStartedActivity();
        assertNotEquals(Intent.FLAG_ACTIVITY_NEW_TASK & intent.getFlags(), 0);
    }

    @Test
    public void skipsInstallIfNotSuccessful() throws Exception {
        enqueResponse(getTestBinary(0));
        task.execute();
        assertNull(shadowApplication.getNextStartedActivity());
    }
}