package com.grubhub.android.orderhub.tasks;

import com.grubhub.android.orderhub.BuildConfig;
import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;
import com.squareup.okhttp.mockwebserver.RecordedRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@Ignore("Don't have a great way of intercepting and mocking http requests. look into that in the future")
public class CaptivePortalCheckTaskTest {
    CaptivePortalCheckTask task;

    Boolean taskResult;

    MockWebServer mockWebServer;
    @Before

    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        mockWebServer = new MockWebServer();
        mockWebServer.start();

        taskResult = null;
        task = new CaptivePortalCheckTask(RuntimeEnvironment.application.getApplicationContext(), new TaskResponseHandler<Boolean>() {
            @Override
            public void handleResponse(Boolean result) {
                taskResult = result;
            }
        });
    }

    @After
    public void shutDown() throws Exception {
        mockWebServer.shutdown();
    }

    private void enqueResponseCode(Integer responseCode){
        mockWebServer.enqueue(new MockResponse().setBody("").setResponseCode(responseCode));
    }

    @Test
    public void hitsGoogle204Endpoint() throws Exception {
        enqueResponseCode(200);
        task.execute();
        RecordedRequest request = mockWebServer.takeRequest();
        assertEquals("GET", request.getMethod());
        assertEquals("http://clients3.google.com/generate_204", request.getPath());
        assertEquals(1, mockWebServer.getRequestCount());
    }

    @Test
    public void returnsTrueIfResponseIsNot204() throws Exception {
        enqueResponseCode(200);
        task.execute();
        assertTrue(taskResult);
    }

    @Test
    public void returnsFalseIfResponseIs204() throws Exception {
        enqueResponseCode(204);
        task.execute();
        assertFalse(taskResult);
    }
}