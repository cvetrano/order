package com.grubhub.android.orderhub.js;

import android.content.Context;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.TestActivity;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.tasks.printing.GetPrinterListTask;
import com.grubhub.android.orderhub.tasks.printing.PrintImageTask;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.PrintResultCode;
import com.grubhub.android.printing.Printer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PrintJsTest extends JsInterfaceBaseTest {
    PrintJs printJs;
    TestActivity activity;

    @Inject
    TaskFactory taskFactory;

    @Mock
    PrintImageTask printImageTask;
    @Mock
    GetPrinterListTask getPrinterListTask;

    @Captor
    ArgumentCaptor<TaskResponseHandler<List<Printer>>> printerListCaptor;
    @Captor
    ArgumentCaptor<TaskResponseHandler<PrintResult>> printImageCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        printJs = new PrintJs(activity, webView);
        when(taskFactory.makePrintImageTask(any(Context.class), any(Printer.class), anyString(), anyInt(), Matchers.<TaskResponseHandler<PrintResult>>any())).thenReturn(printImageTask);
        when(taskFactory.makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any())).thenReturn(getPrinterListTask);
    }

    @Test
    public void init_doesNotResyncCurrentPrinterIfMacNotSet() throws Exception {
        //mac not set in localsettings
        verify(taskFactory, never()).makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any());
    }

    @Test
    public void init_resyncsCurrentPrinterIfMacSet_doesNothingIfMacNotFound() throws Exception {
        String mac = "abc";
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn(mac);
        printJs = new PrintJs(activity, webView);
        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> printerList = new ArrayList<>();
        printerList.add(new Printer("foo", "bar", "baz"));
        printerListCaptor.getValue().handleResponse(printerList);
        assertEquals("{}", printJs.getCurrentPrinter());
    }

    @Test
    public void init_resyncsCurrentPrinterIfMacSet_setsPrinterIfMacFound() throws Exception {
        String mac = "abc";
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn(mac);
        printJs = new PrintJs(activity, webView);
        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> printerList = new ArrayList<>();
        printerList.add(new Printer("foo", mac, "baz"));
        printerListCaptor.getValue().handleResponse(printerList);
        assertEquals(printJs.getPrinterJsonObj(printerList.get(0)).toString(), printJs.getCurrentPrinter());
    }

    @Test
    public void getAvailablePrinters_nullTaskResult_unsuccessfulAsyncResponse() throws Exception {
        String asyncId = printJs.getAvailablePrinters();
        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        printerListCaptor.getValue().handleResponse(null);
        verifyAsyncResponse(printJs, asyncId, printJs.getBaseResponse(false));
    }

    @Test
    public void getAvailablePrinters_emptyListTaskResult_unsuccessfulAsyncResponse() throws Exception {
        String asyncId = printJs.getAvailablePrinters();
        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        printerListCaptor.getValue().handleResponse(new ArrayList<Printer>());
        JSONObject response = printJs.getBaseResponse(false);
        response.put("printers", new JSONArray());
        verifyAsyncResponse(printJs, asyncId, response);
    }

    @Test
    public void getAvailablePrinters_sendsPrinterListViaAsync() throws Exception {
        String asyncId = printJs.getAvailablePrinters();
        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();

        List<Printer> printers = new ArrayList<>();
        printers.add(new Printer("foo", "bar", "baz"));
        printers.add(new Printer("grereg", "fgre", "grerg"));
        printerListCaptor.getValue().handleResponse(printers);

        JSONObject response = printJs.getBaseResponse(true);
        JSONArray printArray = new JSONArray();
        printArray.put(printJs.getPrinterJsonObj(printers.get(0)));
        printArray.put(printJs.getPrinterJsonObj(printers.get(1)));
        response.put("printers", printArray);
        verifyAsyncResponse(printJs, asyncId, response);
    }

    @Test
    public void testSavePrinter_savesMacAddressInLocalStorage() throws Exception {
        printJs.savePrinter("foo", "bar", "baz");
        verify(localSettings).set(PreferenceType.DefaultPrinterMacAddress, "bar");
    }

    @Test
    public void getCurrentPrinter_getsResultOfSavePrinter() throws Exception {
        printJs.savePrinter("foo", "bar", "baz");
        printJs.savePrinter("abc", "123", "baller");
        JSONObject result = printJs.getPrinterJsonObj(new Printer("abc" , "123", "baller"));
        assertEquals(result.toString(), printJs.getCurrentPrinter());
    }

    @Test
    public void printImage_sendsAsyncResponseForSuccessfulPrint() throws Exception {
        String image = "image123";
        Printer printer = new Printer("abc", "123", "ferf");
        printJs.savePrinter("abc", "123", "ferf");
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();

        PrintResult result = new PrintResult();
        result.setSuccess(true);
        printImageCaptor.getValue().handleResponse(result);
        verifyAsyncResponse(printJs, asyncId, printJs.getBaseResponse(true));
    }

    @Test
    public void printImage_sendsAsyncResponseForUnsuccessfulPrint_onlyOnePrintTry() throws Exception {
        String image = "image123";
        Printer printer = new Printer("abc", "123", "ferf");
        printJs.savePrinter("abc", "123", "ferf");
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();

        PrintResult result = new PrintResult();
        result.setSuccess(false);
        result.setResultCode(PrintResultCode.CutterError);
        result.setPrintExceptionMessage("fail 123");
        printImageCaptor.getValue().handleResponse(result);

        JSONObject response = printJs.getBaseResponse(false);
        response.put("resultCode", PrintResultCode.CutterError.toString());
        response.put("exceptionMessage", "fail 123");
        verifyAsyncResponse(printJs, asyncId, response);
    }

    @Test
    public void printImage_resyncPrinterIfOfflineResult_sendsOfflineResultIfResyncFails() throws Exception {
        String image = "image123";
        Printer printer = new Printer("abc", "123", "ferf");
        printJs.savePrinter("abc", "123", "ferf");
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();
        verify(taskFactory, never()).makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any());

        PrintResult result = new PrintResult();
        result.setSuccess(false);
        result.setResultCode(PrintResultCode.PrinterOffline);
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn("abc");
        printImageCaptor.getValue().handleResponse(result);

        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> resyncResult = new ArrayList<>();
        resyncResult.add(new Printer("foo", "bar", "baz"));
        printerListCaptor.getValue().handleResponse(resyncResult);

        JSONObject response = printJs.getBaseResponse(false);
        response.put("resultCode", PrintResultCode.PrinterOffline.toString());
        verifyAsyncResponse(printJs, asyncId, response);
    }

    @Test
    public void printImage_resyncPrinterIfOfflineResult_savesNewPrinterAndRetriesPrintIfResyncSuccessful_sendsAsyncResponseIfRetryFailsOffline() throws Exception {
        String image = "image123", ip = "abc", mac="123", name="ferf";
        Printer printer = new Printer(ip, mac, name);
        printJs.savePrinter(ip, mac, name);
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();
        verify(taskFactory, never()).makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any());

        PrintResult result = new PrintResult();
        result.setSuccess(false);
        result.setResultCode(PrintResultCode.PrinterOffline);
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn(mac);
        printImageCaptor.getValue().handleResponse(result);

        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> resyncResult = new ArrayList<>();
        ip = "new ip";
        resyncResult.add(new Printer(ip, mac, name));
        printer = resyncResult.get(0);
        printerListCaptor.getValue().handleResponse(resyncResult);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask, times(2)).execute();
        printImageCaptor.getValue().handleResponse(result);

        JSONObject response = printJs.getBaseResponse(false);
        response.put("resultCode", PrintResultCode.PrinterOffline.toString());
        verifyAsyncResponse(printJs, asyncId, response);

        JSONObject currentPrinterResult = printJs.getPrinterJsonObj(new Printer(ip, mac, name));
        assertEquals(currentPrinterResult.toString(), printJs.getCurrentPrinter());
    }

    @Test
    public void printImage_resyncPrinterIfOfflineResult_retriesPrintIfResyncSuccessful_sendsAsyncResponseIfRetryFailsNonOfflineError() throws Exception {
        String image = "image123", ip = "abc", mac="123", name="ferf";
        Printer printer = new Printer(ip, mac, name);
        printJs.savePrinter(ip, mac, name);
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();
        verify(taskFactory, never()).makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any());

        PrintResult result = new PrintResult();
        result.setSuccess(false);
        result.setResultCode(PrintResultCode.PrinterOffline);
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn(mac);
        printImageCaptor.getValue().handleResponse(result);

        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> resyncResult = new ArrayList<>();
        ip = "new ip";
        resyncResult.add(new Printer(ip, mac, name));
        printer = resyncResult.get(0);
        printerListCaptor.getValue().handleResponse(resyncResult);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask, times(2)).execute();
        result.setResultCode(PrintResultCode.CutterError);
        printImageCaptor.getValue().handleResponse(result);

        JSONObject response = printJs.getBaseResponse(false);
        response.put("resultCode", PrintResultCode.CutterError.toString());
        verifyAsyncResponse(printJs, asyncId, response);
    }

    @Test
    public void printImage_resyncPrinterIfOfflineResult_retriesPrintIfResyncSuccessful_sendsAsyncResponseIfRetrySucceeds() throws Exception {
        String image = "image123", ip = "abc", mac="123", name="ferf";
        Printer printer = new Printer(ip, mac, name);
        printJs.savePrinter(ip, mac, name);
        String asyncId = printJs.printImage(image, 123);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask).execute();
        verify(taskFactory, never()).makePrinterListTask(any(Context.class), Matchers.<TaskResponseHandler<List<Printer>>>any());

        PrintResult result = new PrintResult();
        result.setSuccess(false);
        result.setResultCode(PrintResultCode.PrinterOffline);
        when(localSettings.getString(PreferenceType.DefaultPrinterMacAddress)).thenReturn(mac);
        printImageCaptor.getValue().handleResponse(result);

        verify(taskFactory).makePrinterListTask(eq(activity), printerListCaptor.capture());
        verify(getPrinterListTask).execute();
        List<Printer> resyncResult = new ArrayList<>();
        ip = "new ip";
        resyncResult.add(new Printer(ip, mac, name));
        printer = resyncResult.get(0);
        printerListCaptor.getValue().handleResponse(resyncResult);

        verify(taskFactory).makePrintImageTask(eq(activity), eq(printer), eq(image), eq(123), printImageCaptor.capture());
        verify(printImageTask, times(2)).execute();
        result = new PrintResult();
        result.setSuccess(true);
        printImageCaptor.getValue().handleResponse(result);

        JSONObject response = printJs.getBaseResponse(true);
        verifyAsyncResponse(printJs, asyncId, response);
    }
}