package com.grubhub.android.orderhub.tasks.printing;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PrintUtils;
import com.grubhub.android.printing.PrintResult;
import com.grubhub.android.printing.PrintResultCode;
import com.grubhub.android.printing.Printer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PrintImageTaskTest {
    PrintImageTask task;

    @Inject
    PrintUtils printUtils;

    PrintResult taskResult;

    final Printer printer = new Printer("foo", "bar", "baz");
    final int numCopies = 23; //for orville and jeremy

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        taskResult = null;
    }

    private void initTask(Printer p, String image){
        task = new PrintImageTask(RuntimeEnvironment.application, p, image, numCopies, new TaskResponseHandler<PrintResult>() {
            @Override
            public void handleResponse(PrintResult result) {
                taskResult = result;
            }
        });
    }

    @Test
    public void nullPrinterCausesPrinterNotFoundResult() throws Exception{
        initTask(null, "");
        task.execute();

        assertFalse(taskResult.isSuccess());
        assertEquals(PrintResultCode.NoPrinterFound, taskResult.getResultCode());
    }

    @Test
    public void emptyImageCausesEmptyPrintImageResult() throws Exception{
        initTask(printer, "");
        task.execute();

        assertFalse(taskResult.isSuccess());
        assertEquals(PrintResultCode.EmptyPrintImage, taskResult.getResultCode());
    }

    @Test
    public void badBase64ImageCausesBadPrintImageResult() throws Exception{
        initTask(printer, "bad base 64");
        task.execute();

        assertFalse(taskResult.isSuccess());
        assertEquals(PrintResultCode.BadPrintImage, taskResult.getResultCode());
    }

    @Test
    public void printsDecodedBase64ImageAndReturnsResult() throws Exception{
        String encodedImage = new Scanner(new File("assets/encodedTestImage")).useDelimiter("\\A").next();
        initTask(printer, encodedImage);

        PrintResult result = new PrintResult();
        result.setResultCode(PrintResultCode.OutOfPaperOrPaperIssues);
        result.setSuccess(false);
        when(printUtils.printImage(any(Context.class), any(Printer.class), any(Bitmap.class), anyInt())).thenReturn(result);
        task.execute();

        byte[] printData = Base64.decode(encodedImage, 0);
        Bitmap printDataBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(printData));

        ArgumentCaptor<Bitmap> bitmapCaptor = ArgumentCaptor.forClass(Bitmap.class);

        verify(printUtils).printImage(eq(RuntimeEnvironment.application), eq(printer), bitmapCaptor.capture(), eq(numCopies));

        assertFalse(taskResult.isSuccess());
        assertEquals(PrintResultCode.OutOfPaperOrPaperIssues, taskResult.getResultCode());
        assertEquals(printDataBitmap.getByteCount(), bitmapCaptor.getValue().getByteCount());
    }
}