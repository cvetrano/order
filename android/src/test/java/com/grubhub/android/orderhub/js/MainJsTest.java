package com.grubhub.android.orderhub.js;

import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.TestActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainJsTest extends JsInterfaceBaseTest {
    MainJs mainJs;
    TestActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        activity = spy(activity);
        mainJs = new MainJs(activity, webView);
    }

    @Test
    public void testGetVersionString() throws Exception {
        String version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        assertEquals(version, mainJs.getVersionString());
    }

    @Test
    public void testGetVersionCode() throws Exception {
        int versionCode = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode;
        assertEquals(versionCode, mainJs.getVersionCode());
    }

    @Test
    public void testReloadPage() throws Exception {
        mainJs.reloadPage();
        verify(activity).runOnUiThread(any(Runnable.class));
        verify(activity).recreate();
    }
}