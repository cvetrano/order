package com.grubhub.android.orderhub.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.R;
import com.grubhub.android.orderhub.util.OrderHubEnvironment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowDialog;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SecretSettingsCustomUrlDialogTest {
    SecretSettingsCustomUrlDialog secretSettingsCustomUrlDialog;
    @Inject
    Context context;
    @Inject
    LayoutInflater layoutInflater;


    SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler emptyHandler = new SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler() {
        @Override
        public void dialogFinished(String gcUrl, String rftHost, String updateBucketUrl) {
        }
    };

    Runnable emptyCancelHandler = new Runnable() {
        @Override
        public void run() {
        }
    };

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);

        secretSettingsCustomUrlDialog = new SecretSettingsCustomUrlDialog(context, layoutInflater);
    }

    @Test
    public void shouldShowDialogAndPopulateWithEnvInfo() throws Exception{
        OrderHubEnvironment env = OrderHubEnvironment.customUrl;
        secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(env, emptyHandler, emptyCancelHandler);

        Dialog dialog = ShadowDialog.getLatestDialog();

        assertTrue(dialog.isShowing());
        assertEquals(env.getGrubcentralUrl(), ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_gc_url)).getText().toString());
        assertEquals(env.getRftHost(), ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_rft_host)).getText().toString());
        assertEquals(env.getUpdateBucketUrl(), ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_update_bucket_url)).getText().toString());
    }

    @Test
    public void shouldDismisslDialogAndRunDialogHandlerOnSubmit() throws Exception {
        final OrderHubEnvironment env = OrderHubEnvironment.customUrl;
        final List<String> testList = new ArrayList<>(); //cant just use a boolean because it needs to be final
        Runnable cancelHandler = new Runnable() {
            @Override
            public void run() {
                testList.add("bad");
            }
        };

        SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler handler = new SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler() {
            @Override
            public void dialogFinished(String gcUrl, String rftHost, String updateBucketUrl) {
                testList.add(gcUrl);
                testList.add(rftHost);
                testList.add(updateBucketUrl);
            }
        };

        secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(env, handler, cancelHandler);

        Dialog dialog = ShadowDialog.getLatestDialog();

        ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_gc_url)).setText("gcurl");
        ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_rft_host)).setText("rfthost");
        ((TextView) dialog.findViewById(R.id.secet_settings_customurl_dialog_enter_update_bucket_url)).setText("updatebucketurl");

        dialog.findViewById(R.id.secet_settings_customurl_dialog_submit_btn).performClick();
        assertFalse(dialog.isShowing());
        assertEquals(3, testList.size());
        assertEquals("gcurl", testList.get(0));
        assertEquals("rfthost", testList.get(1));
        assertEquals("updatebucketurl", testList.get(2));

    }

    @Test
    public void shouldCancelDialogAndRunOnlyCancelHandlerOnCloseButton() throws Exception {
        final OrderHubEnvironment env = OrderHubEnvironment.customUrl;
        final List<Integer> testList = new ArrayList<>(); //cant just use a boolean because it needs to be final
        Runnable cancelHandler = new Runnable() {
            @Override
            public void run() {
                testList.add(123);
            }
        };

        SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler handler = new SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler() {
            @Override
            public void dialogFinished(String gcUrl, String migrationUrl, String rftHost) {
                testList.add(456);
            }
        };

        secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(env, handler, cancelHandler);

        Dialog dialog = ShadowDialog.getLatestDialog();
        dialog.findViewById(R.id.secet_settings_customurl_dialog_close_btn).performClick();
        assertFalse(dialog.isShowing());
        assertTrue(testList.contains(123));
        assertFalse(testList.contains(456));
    }

    @Test
    public void shouldCancelDialogAndRunOnlyCancelHandlerOnBackButton() throws Exception {
        final OrderHubEnvironment env = OrderHubEnvironment.customUrl;
        final List<Integer> testList = new ArrayList<>(); //cant just use a boolean because it needs to be final
        Runnable cancelHandler = new Runnable() {
            @Override
            public void run() {
                testList.add(123);
            }
        };

        SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler handler = new SecretSettingsCustomUrlDialog.SecretSettingsCustomUrlDialogHandler() {
            @Override
            public void dialogFinished(String gcUrl, String migrationUrl, String rftHost) {
                testList.add(456);
            }
        };

        secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(env, handler, cancelHandler);

        Dialog dialog = ShadowDialog.getLatestDialog();
        dialog.onBackPressed();
        assertFalse(dialog.isShowing());
        assertTrue(testList.contains(123));
        assertFalse(testList.contains(456));
    }

    @Test(expected = SecretSettingsCustomUrlDialog.InvalidCustomEnvironmentException.class)
    public void shouldThrowExceptionWhenNonCustomUrlPassed() throws Exception{
        secretSettingsCustomUrlDialog.displaySecretSettingsUrlDialog(OrderHubEnvironment.production, emptyHandler, emptyCancelHandler);
    }
}