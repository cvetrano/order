package com.grubhub.android.orderhub.tasks.printing;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.PrintUtils;
import com.grubhub.android.printing.Printer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class GetPrinterListTaskTest {
    GetPrinterListTask getPrinterListTask;

    @Inject
    PrintUtils printUtils;

    List<Printer> taskResult;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        taskResult = null;
        getPrinterListTask = new GetPrinterListTask(RuntimeEnvironment.application, new TaskResponseHandler<List<Printer>>() {
            @Override
            public void handleResponse(List<Printer> result) {
                taskResult = result;
            }
        });
    }

    @Test
    public void callsTheHandlerWithResultFromGetAvailablePrinters(){
        List<Printer> expectedResult = new ArrayList<>();
        expectedResult.add(new Printer("foo", "bar", "baz"));
        expectedResult.add(new Printer("frsf", "fsfrs", "sffr"));
        when(printUtils.getAvailablePrinters()).thenReturn(expectedResult);

        getPrinterListTask.execute();
        assertEquals(expectedResult, taskResult);
    }
}