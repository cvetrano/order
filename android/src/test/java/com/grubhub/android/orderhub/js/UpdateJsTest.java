package com.grubhub.android.orderhub.js;

import android.content.Context;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.TestActivity;
import com.grubhub.android.orderhub.tasks.DownloadUpdateTask;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.tasks.TaskFactory;
import com.grubhub.android.orderhub.tasks.TaskResponseHandler;
import com.grubhub.android.orderhub.util.RecoveryUtils;
import com.grubhub.android.orderhub.util.UpdateContext;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class UpdateJsTest extends JsInterfaceBaseTest{
    UpdateJs updateJs;
    TestActivity activity;

    @Inject
    TaskFactory taskFactory;
    @Inject
    RecoveryUtils recoveryUtils;

    @Mock
    DownloadUpdateTask downloadUpdateTask;
    @Captor
    ArgumentCaptor<TaskResponseHandler<Boolean>> handlerCaptor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        updateJs = new UpdateJs(activity, webView);
        when(taskFactory.makeDownloadUpdateTask(any(UpdateContext.class), any(Context.class), Matchers.<TaskResponseHandler<Boolean>>any())).thenReturn(downloadUpdateTask);
    }

    @Test
    public void testForceUpdateRecovery_noUpdateContext_postsFailedJsonAsyncResponse() throws Exception {
        when(recoveryUtils.getRecoveryUpdateContext(activity)).thenReturn(null);
        String asyncId = updateJs.forceUpdateRecovery();
        JSONObject expectedResponse = updateJs.getBaseResponse(false);
        expectedResponse.put("error", "recovery update not needed");
        verifyAsyncResponse(updateJs, asyncId, expectedResponse);

    }

    @Test
    public void testForceUpdateRecovery_setsRecoveryUpdateTriedLastSessionOnDownloadSuccess() throws Exception {
        when(recoveryUtils.getRecoveryUpdateContext(activity)).thenReturn(UpdateContext.RECOVERY);
        updateJs.forceUpdateRecovery();
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.RECOVERY), eq(activity), handlerCaptor.capture());
        verify(localSettings, never()).setBoolean(any(PreferenceType.class), anyBoolean());
        handlerCaptor.getValue().handleResponse(false);
        verify(localSettings, never()).setBoolean(any(PreferenceType.class), anyBoolean());
        handlerCaptor.getValue().handleResponse(true);
        verify(localSettings).setBoolean(PreferenceType.RecoveryUpdateTriedLastSession, true);
    }

    @Test
    public void testForceUpdateRecovery_makesDownloadUpdateTaskThatSendsAsyncResponse() throws Exception {
        when(recoveryUtils.getRecoveryUpdateContext(activity)).thenReturn(UpdateContext.RECOVERY_OLD_PACKAGE);
        String asyncId = updateJs.forceUpdateRecovery();
        verify(taskFactory).makeDownloadUpdateTask(eq(UpdateContext.RECOVERY_OLD_PACKAGE), eq(activity), handlerCaptor.capture());
        handlerCaptor.getValue().handleResponse(true);
        verifyAsyncResponse(updateJs, asyncId, updateJs.getBaseResponse(true));
    }

    @Test
    public void testRecoveryUpdateNeeded_trueIfRecoveryUpdateContextIsNotNull() throws Exception {
        when(recoveryUtils.getRecoveryUpdateContext(activity)).thenReturn(UpdateContext.RECOVERY_OLD_PACKAGE);
        assertTrue(updateJs.recoveryUpdateNeeded());
    }

    @Test
    public void testRecoveryUpdateNeeded_falseIfRecoveryUpdateContextIsNull() throws Exception {
        when(recoveryUtils.getRecoveryUpdateContext(activity)).thenReturn(null);
        assertFalse(updateJs.recoveryUpdateNeeded());
    }
}