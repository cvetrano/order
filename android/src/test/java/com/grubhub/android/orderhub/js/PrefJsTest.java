package com.grubhub.android.orderhub.js;

import android.content.SharedPreferences;

import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.TestActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PrefJsTest extends JsInterfaceBaseTest {
    PrefJs prefJs;
    TestActivity activity;

    @Mock
    SharedPreferences mockPrefs;
    @Mock
    SharedPreferences.Editor mockEditor;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        activity = spy(activity);

        Mockito.when(mockPrefs.edit()).thenReturn(mockEditor);
        Mockito.when(activity.getPreferences(anyInt())).thenReturn(mockPrefs);

        prefJs = new PrefJs(activity, webView);
        prefJs = spy(prefJs);
    }

    @Test
    public void getLength_sharedPreferencesInvocation() {
        prefJs.getLength();
        verify(mockPrefs, times(1)).getAll();
    }

    @Test
    public void clear_sharedPreferencesInvocation(){
        prefJs.clear();
        verify(mockPrefs, times(1)).edit();
        verify(mockEditor, times(1)).clear();
        verify(mockEditor, times(1)).commit();
    }

    @Test
    public void getItem_sharedPreferencesInvocation(){
        String key = "foo";
        prefJs.getItem(key);
        verify(mockPrefs, times(1)).getString(eq(key), isNull(String.class));
    }

    @Test(expected = Error.class)
    public void key_sharedPreferencesInvocation() throws Exception{
        verify(prefJs).key(0);
    }

    @Test
    public void removeItem_sharedPreferencesInvocation(){
        String key = "foo";
        prefJs.removeItem(key);
        verify(mockPrefs, times(1)).edit();
        verify(mockEditor, times(1)).remove(eq(key));
        verify(mockEditor, times(1)).commit();
    }

    @Test
    public void setItem_sharedPreferencesInvocation(){
        String key = "foo";
        String value = "bar";
        prefJs.setItem(key, value);
        verify(mockPrefs, times(1)).edit();
        verify(mockEditor, times(1)).putString(eq(key), eq(value));
        verify(mockEditor, times(1)).commit();
    }
}
