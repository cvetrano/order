package com.grubhub.android.orderhub.js;

import android.webkit.WebView;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.util.LocalSettings;

import org.json.JSONObject;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

public abstract class JsInterfaceBaseTest {
    @Inject
    LocalSettings localSettings;
    @Mock
    WebView webView;
    @Captor
    ArgumentCaptor<Runnable> postCaptor;

    protected void verifyAsyncResponse(JsInterfaceBase jsInterface, String asyncId, JSONObject expectedResponse){
        verify(webView).post(postCaptor.capture());
        postCaptor.getValue().run();
        verify(webView).loadUrl(jsInterface.getAsyncResponseString(asyncId, expectedResponse));
    }
}