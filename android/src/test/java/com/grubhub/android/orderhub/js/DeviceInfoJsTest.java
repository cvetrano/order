package com.grubhub.android.orderhub.js;

import com.google.inject.Inject;
import com.grubhub.android.orderhub.BuildConfig;
import com.grubhub.android.orderhub.TestActivity;
import com.grubhub.android.orderhub.util.DeviceUtils;
import com.grubhub.android.orderhub.util.PreferenceType;
import com.grubhub.android.orderhub.util.LocalSettings;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DeviceInfoJsTest extends JsInterfaceBaseTest{
    DeviceInfoJs deviceInfoJs;
    TestActivity activity;

    @Inject
    DeviceUtils deviceUtils;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        activity = Robolectric.buildActivity(TestActivity.class).create().start().get();
        deviceInfoJs = new DeviceInfoJs(activity, webView);
    }

    @Test
    public void testMarkDeviceMigrated() throws Exception {
        deviceInfoJs.markDeviceMigrated();
        verify(localSettings).setBoolean(PreferenceType.HasMigrated, true);
    }

    @Test
    public void testGetMacAddress() throws Exception {
        when(deviceUtils.getMacAddress(eq(activity))).thenReturn("foobar");
        assertEquals("foobar", deviceInfoJs.getMacAddress());
    }

    @Test
    public void testIsCharging() throws Exception {
        when(deviceUtils.isCharging(eq(activity))).thenReturn(true);
        assertTrue(deviceInfoJs.isCharging());
    }

    @Test
    public void testGetBatteryLevel() throws Exception {
        when(deviceUtils.getBatteryLevel(eq(activity))).thenReturn(50);
        assertEquals(50, deviceInfoJs.getBatteryLevel());
    }

    @Test
    public void testIsScreenOn() throws Exception {
        when(deviceUtils.isScreenOn(eq(activity))).thenReturn(false);
        assertFalse(deviceInfoJs.isScreenOn());
    }

    @Test
    public void testIsGCOpen() throws Exception {
        when(deviceUtils.isAppOpen()).thenReturn(true);
        assertTrue(deviceInfoJs.isGCOpen());
    }
}