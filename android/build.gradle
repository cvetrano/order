plugins {
    id "com.github.ethankhall.semantic-versioning" version "1.1.0"
    id "com.jfrog.artifactory" version "3.0.1"
}

apply plugin: "com.grubhub.build.grubpass.artifactory"
apply plugin: "com.android.application"
apply plugin: 'maven-publish'
apply plugin: 'base'
apply plugin: 'io.fabric'

int versionMajor = 7
int versionMinor = 5  //version minor can't go above 100
int versionPatch = 0  //BRING THIS BACK WHEN WE CAN BUILD ON JENKINS (System.getenv("BUILD_NUMBER") ?: 0) as Integer

version.with {
    major = versionMajor;
    minor = versionMinor;
    patch = versionPatch;
    releaseBuild = true;
}

def buildArtifact = file('build/outputs/apk/android-release.apk')

publishing.publications {
    app(MavenPublication) {
        artifactId 'android-orderhub'
        artifact buildArtifact
    }
}

android {
    compileSdkVersion 24
    buildToolsVersion = '27.0.3'

    sourceSets {
        main {
            manifest.srcFile 'AndroidManifest.xml'
            res.srcDirs = ['res']
            assets.srcDirs = ['assets']
        }
    }

    defaultConfig {
        //the multipliers prevent conflicts from using the ever increasing jenkins build number.
        versionCode versionMajor * 100000000 + versionMinor * 1000000 + versionPatch
        versionName version.toString()
        minSdkVersion 17
        //noinspection OldTargetApi
        targetSdkVersion 23
        jackOptions {
            enabled false
        }
    }

    signingConfigs {
        release {
            storeFile file('grubHub.keystore')
            storePassword 'al8dz39inmasd341'
            keyAlias 'grubHub'
            keyPassword 'al8dz39inmasd341'
        }
    }

    buildTypes {
        release {
            signingConfig signingConfigs.release
        }
    }

    // these text files exist in multiple jars
    packagingOptions {
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/maven/com.google.guava/guava/pom.properties'
        exclude 'META-INF/maven/com.google.guava/guava/pom.xml'
    }

    lintOptions {
        ignore 'ResAuto'
        // TODO replace all the xmlns:oh URLs with "http://schemas.android.com/apk/res-auto"
        abortOnError false
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_7
        targetCompatibility JavaVersion.VERSION_1_7
    }
}


build.doLast {
    def file = new File(buildDir.path + "/outputs/apk", "gcinabox.versioncode");
    file.write(android.defaultConfig.versionCode.toString());
}

repositories {
    mavenCentral()
    jcenter()
    maven { url 'https://maven.fabric.io/public' }
    maven {
        url getProperty('artifactory.server') + '/maven-thirdparty-local'
    }
    maven {
        url getProperty('artifactory.server') + '/grubhub-merged'
    }
}

dependencies {
    provided 'org.roboguice:roboblender:3.0'
    compile('org.roboguice:roboguice:3.0') {
        exclude module: 'asm'
    }
    compile 'org.apache.commons:commons-lang3:3.1'
    compile 'com.android.support:support-v4:24.2.0'
    compile 'com.google.guava:guava:20.0'
    compile 'joda-time:joda-time:1.6.2'
    compile 'com.starmicronics.stario:StarIOPort:3.1'
    compile('com.crashlytics.sdk.android:crashlytics:2.6.5@aar') {
        transitive = true;
    }

    testCompile 'junit:junit:4.12'
    testCompile 'org.mockito:mockito-core:1.10.19'
    testCompile 'org.hamcrest:hamcrest-library:1.3'
    testCompile 'org.robolectric:robolectric:3.2'
    testCompile 'com.squareup.okhttp:mockwebserver:2.5.0'
    testCompile 'org.khronos:opengl-api:gl1.1-android-2.1_r1'
    testCompile 'com.android.support.test.uiautomator:uiautomator-v18:2.1.3'
}

def server = getProperty('artifactory.server')
def repo = server.contains("https://artifactory.grubhub.com") ? 'ci-testing-release' : 'grubhub-snapshot'


artifactory {
    contextUrl = server
    publish {
        repository {
            repoKey = repo
        }
        defaults {
            publications('app')
            publishBuildInfo = true;
            publishArtifacts = true;
        }
    }
    resolve {
        repository {
            repoKey = 'grubhub-merged'
        }
    }
}

task wrapper(type: Wrapper) {
    gradleVersion = '4.4'
}