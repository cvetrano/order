#
# Screen shot of the usage:
# Password: password
#
#
#keytool -genkey -v -keystore dev.orderhub.keystore -alias orderhub -keyalg RSA -keysize 2048 -validity 50000
#ychiu@yuan-dev:~/dev/orderhub$ ./genCert.sh 
#Enter keystore password:  
#Re-enter new password: 
#What is your first and last name?
#  [Unknown]:  Zoidberg
#What is the name of your organizational unit?
#  [Unknown]:  Dev
#What is the name of your organization?
#  [Unknown]:  GrubHub
#What is the name of your City or Locality?
#  [Unknown]:  Chicago
#What is the name of your State or Province?
#  [Unknown]:  IL
#What is the two-letter country code for this unit?
#  [Unknown]:  US
#Is CN=Zoidberg, OU=Dev, O=GrubHub, L=Chicago, ST=IL, C=US correct?
#  [no]:  yes
#
#Generating 2,048 bit RSA key pair and self-signed certificate (SHA1withRSA) with a validity of 50,000 days
#	for: CN=Zoidberg, OU=Dev, O=GrubHub, L=Chicago, ST=IL, C=US
#Enter key password for <orderhub>
##	(RETURN if same as keystore password):  
#[Storing dev.orderhub.keystore]
#ychiu@yuan-dev:~/dev/orderhub$ 
#
#

keytool -genkey -v -keystore dev.orderhub.keystore -alias orderhub -keyalg RSA -keysize 2048 -validity 50000

